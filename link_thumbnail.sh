#!/bin/bash
IMG=$1
WIDTH=$2
PREFIX=thumbnail_
TIMES=x
echo  $PREFIX$IMG
convert $IMG -resize $WIDTH$TIMES$WIDTH $PREFIX$IMG
pwd
mv -f $PREFIX$IMG html/assets/
cp $IMG html/assets/$IMG
