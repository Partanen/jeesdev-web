title = A generic hashtable in C
author = Kuvis

<P>
When ever I've implemented hashtables in C in the past, the way I've made them
generic has been to create macros that declare a new types for the tables
wanted, and sets of functions to manipulate them. For example, taken from the
MUTA source code:
</P>

<DIV CLASS="post_code">
#define DYNAMIC_HASH_TABLE_DEFINITION(table_name, value_type, key_type, \<BR>
    hash_type, hash_func, bucket_sz) \<BR>
    /* ... */ \<BR>
<BR>
...<BR>
<BR>
DYNAMIC_HASH_TABLE_DEFINITION(str_int_table, int, char *, uint32,<BR>
    fnv_hash32_from_str, 4);
</DIV>

<P>
With such macros, it's been a big bother to have to declare each hashtable
type before using it. However, <A HREF="https://github.com/nothings/stb">
the stb library</A> implements generic dynamic arrays with macros trickery, so
why not do the same with hashtables?
</P>

<P>
Turns out working with a more complex structure like a hashtable takes a
little more work than dynamic arrays. We cannot automatically create versions
of a single function for different data types in C like we could with C++
templates, so the main problem is passing compile-time data such as struct
member types and sizes to functions. And we do want to use functions because
the we will need complex statements such as loops that cannot be fit in one
line and bracketed with other statements.
</P>

<P>
The above paragraph might not have made that much immediate sense, so allow me
to attempt to clarify my point. Below is my generic hashtable declaration
function.
</P>

<DIV CLASS="post_code">
#define hashtable(key_type, value_type) \<BR>
    struct { \<BR>
        struct { \<BR>
            struct { \<BR>
                key_type    key; \<BR>
                value_type  value; \<BR>
                size_t      hash; \<BR>
                uint8_t     reserved; \<BR>
            } items[HASHTABLE_BUCKET_SIZE]; \<BR>
        } *buckets; \<BR>
        size_t num_buckets; \<BR>
        size_t num_values; \<BR>
    }
</DIV>

<P>
Below is how it's used.
</P>

<DIV CLASS="post_code">
/* Declare a hashtable called 'table' whose key type is char * and value type
 * is int. */
hashtable(char *, int) table;
</DIV>

<P>
As may be observed, the type of the resulting struct is anonymous, meaning it
cannot even be passed to a struct as a void * and then cast back to its
original type. So how do we do that? Let me show you the hashtable_insert_ext()
macro and the signature of the _hashtable_insert() function the macro calls.
</P>

<DIV CLASS="post_code">
#define hashtable_insert_ext(table_, pair_, compare_keys_func_, \<BR>
    copy_key_func_, ret_err_) \<BR>
    (table_.buckets = _hashtable_insert((ret_err_), \<BR>
        (uint8_t*)table_.buckets, &table_.num_buckets, &table_.num_values,
        \<BR>
        sizeof(*table_.buckets), sizeof(table_.buckets[0].items[0]), \<BR>
        _hashtable_ptr_offset(&table_.buckets[0].items[0], \<BR>
            &table_.buckets[0]), \<BR>
        _hashtable_ptr_offset(&table_.buckets[0].items[0].key, \<BR>
            &table_.buckets[0].items[0]), \<BR>
        _hashtable_ptr_offset(&table_.buckets[0].items[0].value, \<BR>
            &table_.buckets[0].items[0]), \<BR>
        _hashtable_ptr_offset(&table_.buckets[0].items[0].hash, \<BR>
            &table_.buckets[0].items[0]), \<BR>
        _hashtable_ptr_offset(&table_.buckets[0].items[0].reserved, \<BR>
            &table_.buckets[0].items[0]), \<BR>
        &pair_.key, sizeof(pair_.key), pair_.hash, &pair_.value, \<BR>
        sizeof(pair_.value), compare_keys_func_, copy_key_func_))<BR>
<BR>
void *_hashtable_insert(int *ret_err, uint8_t *buckets, size_t *num_buckets,<BR>
    size_t *num_values, size_t bucket_size, size_t item_size,<BR>
    size_t items_offset, size_t key_off1, size_t value_off1, size_t
    hash_off1,<BR>
    size_t reserved_off1, void *key, size_t key_size, size_t hash, void
    *value,<BR>
    size_t value_size,<BR>
    int (*compare_keys)(const void *a, const void *b, size_t size),<BR>
    int (*copy_key)(void *dst, const void *src, size_t size));
</DIV>

<P>
Yep, instead of passing in a struct to _hashtable_insert(), we pass pointers
to each of the struct's relevant members. Sizes of some data types are also
passed, and finally, struct offsets using the macro _hashtable_ptr_offset().
That allows us to modify everything at the byte level, which we do inside
_hashtable_insert().
</P>

<P>
One may wonder if this is an efficient way to do things. I would say it
probably isn't, not in terms of runtime efficiency anyway. But in terms of
programmer efficiency, I think it might be the right choice until I come
across a better solution. That's because with the this implementation, doing
things such as the following is super easy compared to what I used to have to
do.
</P>

<DIV CLASS="post_code">
hashtable(uint32_t, int) table;<BR>
hashtable_init(table, 8, 0);<BR>
<BR>
struct {<BR>
    uint32_t    key;<BR>
    int         value;<BR>
    size_t      hash;<BR>
} insert_data = {123, 5, hash_u32(123)};<BR>
hashtable_insert(table, insert_data);<BR>
<BR>
struct {<BR>
    uint32_t    key;<BR>
    size_t      hash;<BR>
} find_data = {123, hash_u32(123)};<BR>
int *value = hashtable_find(table, find_data); <BR>
if (value) {<BR>
    ...<BR>
}
</DIV>

<P>
Okay, maybe it isn't the most intuitive API ever, but I like it far more than
having to write type/function declaration macros like I used to. And for my
hashtable use-cases, I'm willing to pay for the potential runtime-overhead.
</P>

<P>
Also, I didn't actually have a hashtable that stored the actualy keys in MUTA
yet. While not storing keys is fine for assets and similar items to save on
memory, for runtime ID's a table like this will be required.
</P>

<P>
I've been working on this over the weekend and it likely still has some bugs
in it, but in case you're curious, the Git repository can be found <A
HREF="https://gitlab.com/Partanen/kuvis_hashtable">here</A>.
I've also uploaded the current header and source files
<A HREF="assets/text/hashtable_02_03_2019.h">here</A> and
<A HREF="assets/text/hashtable_02_03_2019.c">here</A>, respectively. I'll keep
on work on this and start using it in MUTA
soon.
</P>
