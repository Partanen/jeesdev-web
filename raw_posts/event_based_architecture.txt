title = Event-based server architecture
author = Kuvis

<P>
What's the right way to do multithreading? There isn't a single answer to this
question of course, as every program has it's specific needs and purposes. But
on a per-program basis, one approach will probably be better than another.
</P>

<P>
Game developers not working on high-end engines rarely have to worry about
multithreading. Even if a game is multithreaded, at the core most games' logic
is single-threaded.
</P>

<P>
For instance, if you have a dedicated rendering thread, your next last frame
might be getting rendered on another thread while you simulate the current one
on the main thread, but in the end what you're doing is you're just
replicating the relevant data for use by the render thread, thus affecting no
data relevant to the main thread where the actual game logic runs.
</P>

<P>
To give another example, your physics system might parallelize a for-loop to
multiple threads, all of which are synchronized at the end of the loop, but
this also does not change your game's logic from single- to multithreaded.
</P>

<P>
There's a reason the above two examples are common in games, and it is that
they are easy to program for, while getting some of the benefits of dividing
work to multiple threads. When all of the real logic happens on a single
thread of execution, the control flow is predictable and easy to understand.
Often it can also be more efficient than a truly multithreaded alternative.
</P>

<H3>Networking and threads</H3>
<P>
While you can well write a game using just a single thread, writing network
code without threads is more difficult. This is because in network programming
we use many <I>blocking calls</I>, such as <A
HREF="https://en.wikipedia.org/wiki/Poll_(Unix)">poll()</A> or recvfrom().
Usually, we want a thread to sit there and wait until messages or new
connections arrive. But a sleeping thread cannot do much else, so the other
logic must happen on another thread.
</P>

<P>
Actually, I'm ignoring one fact above. It is usually possible to give blocking
functions a timeout, so that even if they have no new events to report to us,
the function will return after a given amount of time. In this way, one can
imagine a game-loop-like server architecture that polls for messages and new
connections on a single thread, all the while also executing all of the logic
on the same thread as well. With a timed wait, we could have the loop run at
approximately, say, 60 FPS.
</P>

<P>
But servers that want scalability probably don't run on a single thread. I'm
no expert of course, so take this with a grain of salt. But so my
investigations suggest, nevertheless. It is rather common to have threads
dedicated simply to polling for new network events.
</P>

<H3>State makes things difficult</H3>
<P>
If the only job of one of our threads is to sleep on poll(), epoll() or
similar and wait for new incoming messages and connections, the logical
conclusion is that the incoming messages and connections will be handled
somewhere else, that is, on a different thread.
</P>

<P>
If a server has long-lasting connections, it likely has to store some kind of
per-client state: the address or socket of the client, a session id,
incomplete messages, game character state... whatever. For that, we might have
a struct called client_t.  And of course, we have some kind of a container
that contains those structs.  Lets say the structs are stored in a pool of a
fixed size.
</P>

<DIV CLASS="post_code">
typedef struct client_t
{
    int     socket;
    uint32  session_id;
    uint8   unread_messages[512];
    int     num_bytes_unread;
} client_t;

client_pool_t clients;

/* Returns NULL if no free clients are left in the pool */
client_t *new_client(client_pool_t *pool);
void free_client(client_pool_t *pool, client_t *client);
</DIV>

<P>
Now a quiz: if all of our connections need to be stored in our global
client_pool_t data structure, how do we access it safely from multiple
threads? Remember that the thread that accepts new clients (the one sleeping
on poll) does not handle any of the actual logic of our program.
</P>

<P>
The naive answer is to just use a mutex. Wrap the new_client() and
free_client() calls to a mutex and you're fine. Now when the network thread
accepts a new client, it can safely call new_client() and then somehow signal
the main thread that a new client was accepted.
</P>

<P>
There are two problems with this approach: the possible performance concerns,
and the (in my opinion) more important topic of code complexity. These
problems are somewhat intertwined.
</P>

<P>
Say that you've gone with the mutex approach, and at some point in development
you realize that there are situations where, on your main thread, you have to
iterate through all of your clients. For that, you might create an array of
pointers to the currently used client structs. So now, when you accept a new
client andreserve a new client struct from the pool, you have to also push a
pointer to the struct to said array.
</P>

<P>
But now, what might happen is that while the main thread iterates through all
of the clients, one of the clients disconnects and the networking thread is
informed of this. So, the networking thread frees the client struct of the
disconnected client. Since we don't want this to happen while we're iterating
through all of the clients, we have to lock the pool's mutex in both cases:
while iterating, and while deleting a client.
</P>

<P>
It's easy to see what the above can lead to: the iteration can leave the mutex
locked for a while, meaning the networking thread gets blocked until the
iteration finishes.
</P>

<P>
But what I think if we do things as we do them above, we have to constantly
remind our selves of the fact we can't just go doing anything to our clients
on the main thread without remembering the mutex. The code becomes more and
more complicated, and easier to make a mistake at. The above was a simple and
somewhat stupid example, but consider what happens if the networking thread
reads into recvd() data into client's buffer, and the main thread handles that
data. Are you going to use a per-client mutex to protect access to the data
buffers? That's gonna be some heavy locking.
</P>

<P>
I'm speaking from (some) experience here. When I started writing the <A
HREF="gitlab.com/Partanen/MUTA">MUTA</A> server, I had little clue about how
to write a complex server capable of handling a high amount of clients.  I'm
not sure if I still have an idea of that, but I know that multithreading was a
pain in the ass for me for a long while, and to some extent it remains so,
although I think it's gettin getter.
</P>

<H3>Handling state on a single thread</H3>

<P>
So far I've found that the simplest way of dealing with the above woes is to
be simple and handle all state changes on on a single thread. No allocations
of stateful data structures or otherwise manipulating them from the networking
thread or anywhere else. I've referred to this as <I>event-based
architecture</I>, but I'm not sure if that's the correct term; all I know is
that I've heard the term elsewhere and assumed this was what they meant.
Then again, who cares what it's called.
</P>


<P>
In this model, when a network thread receives a message, it will not handle
the message immediately. Instead it will pass it on to another thread as an
event. The thread that handles the events (usually the main thread) can sleep
on a blocking function from which it wakes when new events are posted from
another thread.
</P>


<A HREF="assets/event_arch_thread_graph.png">
<IMG CLASS="article_thumbnail" SRC="assets/thumbnail_event_arch_thread_graph.png">
</A>

<P>
Essentially, both the main and network thread will sleep in this model until
something relevant to them happens. Of course, you can have more than one
thread pushing events to the main thread though, and you can also have the
main thread posting events to other threads. MUTA's login server has a
database thread with which the main thread communicates in a two-way fashion,
posting and receiving events from it.
</P>

<H3>Current implementation in MUTA</H3>

<P>
MUTA's server consists of multiple programs now: the login server, the shard
server, the world simulation server, the database server and the proxy server.
The two newest applications, the login and proxy servers, implement the
architecture described in this post.
</P>

<P>
The sleeping and waking of a thread is implemented using a condition variable
that sleeps on a mutex. The same mutex is locked when items are pushed to the
event queue.
</P>

<P>
The event queue is also a blocking queue. It has a fixed size, and if it fills
up, meaning the thread pushing events is pushing them faster than the main
thread can handle them, the push function will sleep until space becomes free.
This is also implemented using a condition variable.
</P>

<P>
The event data type of the MUTA login server looks as follow at the moment.
</P>

<DIV CLASS="post_code">
enum event_type
{
    EVENT_ACCEPT_CLIENT,
    EVENT_READ_CLIENT,
    EVENT_ACCOUNT_LOGIN_QUERY_FINISHED,
    EVENT_ACCEPT_SHARD,
    EVENT_READ_SHARD,
    EVENT_LOGIN_REQUEST_RESULT
};

... various event structures ...

struct event_t
{
    union
    {
        int                                     type;
        accept_client_event_t                   accept_client;
        read_client_event_t                     read_client;
        account_login_query_finished_event_t    account_login_query_finished;
        accept_shard_event_t                    accept_shard;
        read_shard_event_t                      read_shard;
    };
};
</DIV>

<P>
As an example event, we can take the accept_client event, which is fired when
the network thread accept()s a new connection. It looks as follows.
</P>

<DIV CLASS="post_code">
struct accept_client_event_t
{
    int         type;
    socket_t    socket;
    addr_t      address;
};
</DIV>

<P>
As we can see, the event data type is a union. That's because they all get
posted into the same array. But what about events that have to transfer
variable amounts of data from thread to thread, such as socket recvs() (that
is, packets received from the network)? In such cases, we use a simply
mutex-locked allocation pool. Lock the allocator's mutex, allocate data,
unlock the mutex. At read, we free the allocated memory back to the allocator
in a similar fashion.
</P>

<P>
The allocator used here is a simple segregate fit -type of allocator. The
logic is this: on malloc-requests, round up the requested amount to the
nearest power of two. Find out the highest (leftmost) bit of the amount and
use it as an index into an internal pool of memory blocks of the given size.
</P>

<A HREF="assets/event_arch_segregated_fit_allocation.png">
<IMG CLASS="article_thumbnail"
SRC="assets/thumbnail_event_arch_segregated_fit_allocation.png">
</A>

<P>
While the allocator calls real malloc when it needs more memory, it has an
upper capacity to which it will allocate, since the event queue is blocking.
Thus it is not possible to DDOS the server to run out of memory by simply
spamming packets at it.
</P>

<P>
The event API itself looks as follows.
</P>

<DIV CLASS="post_code">
typedef struct event_buf_t event_buf_t;

void event_init(event_buf_t *buf, uint32 item_size, int32 max);
void event_push(event_buf_t *buf, void *evs, int32 num);
int event_wait(event_buf_t *buf, void *evs, int32 max, int timeout_ms);
/* -1 as timeout means infinite */
</DIV>

<P>
And, using the API, the login server's main loop looks as follows now.
</P>

<DIV CLASS="post_code">
for (;;)
{
    event_t events[MAX_EVENTS];
    int num_events = event_wait(event_buf, _events, MAX_EVENTS, 500);
    for (int i = 0; i < num_events; ++i)
    {
        event_t *e = &_events[i];
        switch (e->type)
        {
        case EVENT_ACCEPT_CLIENT:
            cl_accept(&e->accept_client);
            break;
        case EVENT_READ_CLIENT:
            cl_read(&e->read_client);
            break;
        case EVENT_ACCOUNT_LOGIN_QUERY_FINISHED:
            cl_finish_login_attempt(&e->account_login_query_finished);
            break;
        case EVENT_ACCEPT_SHARD:
            shards_accept(&e->accept_shard);
            break;
        case EVENT_READ_SHARD:
            shards_read(&e->read_shard);
            break;
        default:
            muta_assert(0);
        }
    }
    cl_check_timeouts();
    shards_flush(); // Flush queued messages to shards
    cl_flush();     // Flush queued messages to clients
}
</DIV>

<P>
And so, most logic is handled on a single thread.
</P>

<H3>Converting other systems to the same approach</H3>
<P>
The MUTA server's older parts are not implemented using the model discussed in
this post. Although they also handle most logic on a single thread based on
events generated by other threads, the model they use is not quite so refined.
Instead it is kind of an ad-hoc implementation of the same idea, so instead of
having a single event buffer, there are many. These buffers are not blocking
like in the case of the login server how ever.
</P>

<P>
The old parts of the server are also frame-based, in that their main thread
runs the logic in specified increments of time (FPS, frames per second). With
an event queue the thread can sleep on we can remove some of the latency
provided by the frame-based approach. To make timers based on delta-time still
function correctly, we can simply calculate a sleep-time for the event_wait()
function much like we would sleep during a frame if we were waiting for vsync
in 3D program.
</P>

<P>
I'll be looking at converting the old programs to this model at some point in
the distant future, unless I come across something better
</P>.
