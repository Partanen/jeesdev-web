title = On Finland deporting Russian diplomats
author = Kuvis

<P>
Note: this post contains personal opinions reflective only of the author's
views and not necessarily of anyone else who partakes in the upkeep of this
website.
</P>

<P>
Often times nowadays, politicians in democratic societies appear as if they
have completely abandoned ideology. Ideology is often frowned upon, at least
if it isn't of the kind that emphasizes the growth of the markets, increasing
the amount of foreign investments in the country, or something in that
fashion. But emphasizing these points, of course, is not viewed as ideology at
all - it is merely viewed as <I>pragmatism</I> through the narrow lens of
market centricism.
</P>

<P>
But sometimes ideology still makes an appearance. You've probably heard of
many EU countries deporting Russian diplomats because of the poisoning of
Sergei Skripal in Britain, an incident the culprit of, the UK and US claim,
is Russia. Finland is amongst those countries.
</P>

<P>
Now, I don't know who poisoned Skripal. But I believe that neither do Finnish
officials. All they have, at least so I believe, is the word of two NATO
countries, the US and the UK. And how trustworthy exactly is that word? 
</P>

<P>
In the traditional nationalist Russia-fearing Finnish mindset where everything
that comes from the west is superior, held on to by a certain portion of the
population, the word of said countries is apparently very trustworthy, at
least if it's an excuse to provoke the grand eastern neighbour (which is
rather analogous to playing with fire). Never you mind the UK only gave Russia
a single day to respond to the accusations they made, or the fact no evidence
of Russian interference has been released to the public.
</P>

<P>
I feel as if Finnish politicians, those who are afraid of Russia at least, are
forgetting what the best strategy has always been for "defense" in this small
country between two great powers: neutrality. And diplomacy. Not only are we
now making decisions based on no evidence, but we're risking our eastern
relationship in the name of the ideology of <I>West is Best</I>, and not even
for the first time.
</P>

<P>
Of course, if it is proven Russia was behind the attack, it's a different
matter. But this hasn't happened, not so far at least.
</P>
