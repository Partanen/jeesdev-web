id = 39
date = 28-9-2019 20:56:27
title = OK hand gesture considered harmful
author = Kuvis

[begin_content]
<P>
It has come to our attention that, according to some, <A
HREF="https://yle.fi/uutiset/3-10994606">the OK hand gesture is now
associated with hate groups</A>. We use the sign in our current logo
logo and do not associate it with any particular ideology. Consider
this a sort of protest against suddenly changing the meaning of a commonly
used hand sign if you want to see a deeper meaning behind it.
</P>
[end_content]
