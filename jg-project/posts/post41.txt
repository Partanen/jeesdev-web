id = 41
date = 30-9-2019 21:41:29
title = MUTA devlog 6: the backend rewrite
author = Kuvis

[begin_content]

<P>
I'm pretty sure it started with a need to rewrite the client's entity
system and rendering. That was about half a year ago - since then, I've only been
working inside feature branches instead of the MUTA main branch,
<I>development</I>. Somehow, one change followed another, and I got carried
away. There was a long time when the main components of the server would not
even run and a much longer period during which the client was not capable of
forming a connection with the server. But that's finally behind now.
</P>

<P>
In the
<A HREF="http://jeesdev.fi/html/post34_mutadevlogsolodevcliententitysystemmasterserverrewriteworlddatabaseserver.html">
last devlog</A> I listed most of features I have been working on. Some newer ones include:
<UL>
<LI>Simulation server rewritten from scratch</LI>
<LI>New interest management system on the master server (now with interest lists rather than just a grid)</LI>
<LI>Moved many server components' protocols to use the new MUTA Packetwriter2 for serialization.</LI>
<LI>Precompiled windows MSVC dependencies and added them to the main repo (wanna do the same for GNU/Linux).</LI>
<LI>Removal of the old database server application.</LI>
</UL>
</P>

<H3>Simulation server rewrite in preparation for clustering</H3>

<P>
The simulation server (previously called <I>worldd</I>, renamed to <I>sim</I>)
was the most ad-hoc piece of the backend in addition to the old database server.
From the features it used to have, only pathfinding remains missing in the new
version - I'll probably just copy-paste it.
</P>

<P>
The original sim server had been built quickly, mostly ignoring the reason it
even existed as a separate application from the master server: clustering.
Each game world is intended to have many simulation servers, each simulating a
separate piece of the land. Now, things have been properly prepared for that.
</P>

<P>
Initially we wanted to use Lua for the sim server's scripting language, but
now I am leaning more towards C. Lua has some advantages, but a separate
scripting language also adds another layer of complexity - developers must
learn two languages, and an API is required to communicate between the
languages. And of course, C is more powerful. So, Lua on the sim server is
gone with the rewrite. I plan on making the C scripts a separate module, so
that in theory one could easily build two different versions of MUTA with
completely different scripts, but still wish to link everything statically.
</P>

<H3>Interest lists</H3>

<P>
Interest management is the act of deciding what objects players receive what
updates from, usually based on distance, being in a party, guild, or something
else.
</P>

<P>
MUTA is a tile based game, so initially I felt a grid-based approach would
suit it naturally. It would save the memory, too. The world was divided into
(IIRC) 16x16x8 tiles large cells, and players only received updates for
objects in their own and the 26 surrounding cells. As I was rewriting the
master server (which handles interest management), I decided to revert this.
My gut says that iterating through 27 cells each time something happens, some
of which might be empty, is no good for performance.
</P>

<P>
Interest lists are data structures that list any players that are interested
in a particular object. Currently the only object types we have are players
and static objects, the latter of which cannot be updated, so it is only
players who get these lists assigned to them. The old-style grid is still
there, but it's only used to update interest lists when objects move. So,
if the master server's object view distance is 16 tiles, any objects within
that distance from a player will be on that player's interest list, unless
they're hidden by a spell or something. And if that player, say, casts a
spell, we can walk through the player's interest list and only send the
casting update to the players on it.
</P>

<H3>Precompiled dependencies</H3>

<P>
I hate 3rd party dependencies. I mean, I like the fact they make development
easier, and I love the people who make them. But they are often annoying to
manage.
</P>

<P>
To combat complexities like compiling/installing big libraries whose version
might not even be right, I have precompiled or downloaded and added into our
Git repository all of the 3rd party libraries we use on Windows. 
</P>

<P>
I'm hoping to do so on GNU/Linux soon also, though it's a little more
complicated there due to the variety of different systems. Unfortunately for
now, we still have some dependencies that are in the repo as Git dependencies
(from external repositories), some of which even require terrible build
systems such as CMake to build.
</P>

<H3>Content and gameplay, coming?</H3>

<P>
With so much base-building behind, I'm feeling pretty confident about getting
to gameplay programming fairly soon (that's what they all say though, right?).
Programming of course isn't enough to make good content. In particular, the
game still lacks graphics, so I'm on the lookout for someone capable of
creating art true to the spirit of the game's world. We'll see about that.
</P>
[end_content]
