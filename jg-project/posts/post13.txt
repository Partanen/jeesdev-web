id = 13
date = 27-3-2018 10:4:43
title = Not rewriting it in Rust, and on Unity's C# source release
author = Kuvis

[begin_content]

<H3>Ain't not rewriting it in Rust</H3>

<P>
Who hasn't heard the phrase "rewrite it in Rust"? I certainly have, and
yesterday finally thought, "you know, maybe I should give the language a try -
a safer version of C sounds pretty tempting".
So I installed rustc, opened a quick tutorial from the web and dived in at the
shallow end.
</P>

<P>
I got as far as structs in about 5 minutes. So off I went and declared my
struct:

<DIV CLASS="post_code">
struct s { <BR>
    x: i32, <BR>
    y: i32 <BR>
}
</DIV>

All well and good I thought, but the compiler decided to be nasty. It told me:

<DIV CLASS="post_code">
warning: type `s` should have a camel case name such as `S`
</DIV>
</P>

<P>
I'm sorry, but I don't think its the job of the language to tell me how to
name my data structures or variables, other than for things like whether they
can include spaces/numbers or not, etc. I can go along with indentation
marking scope, like in Python or other languages - I believe that's been
proven to improve readability across the board. But I'm not interested in
styling my naming conventions after the personal tastes of the creator of the
language.
</P>

<P>
I realize styling warnings can be disabled via compiler options, but if its in
the standard, you probably shouldn't. So I guess that's it for Rust from my
part for now.
</P>

<H3>Unity's C# source code reference-only release</H3>
<P>
Unity recently decided to <A
HREF="https://blogs.unity3d.com/2018/03/26/releasing-the-unity-c-source-code/">
release it's engine and editor C# source code</A>. The C++ core of
the engine remains out of sight, and even the C# release is made under
a reference-only license, meaning that modifying the source is not allowed.
So, unfortunately we don't come even close to fulfilling the <A
HREF="https://en.wikipedia.org/wiki/The_Free_Software_Definition#The_definition_and_the_Four_Freedoms">four
software freedoms</A> as traditionally defined. Unity seems to stress this
themselves in the blog post. And of course, one may argue this means little
since the C# source code could already be decompiled by parties interested.
But I won't deny it's still a very small step in the right direction.
</P>

<P>
The release, I think, is not that big of a deal though. Unortunately, Unity
still appears to hold some old-fashioned views popular in the
proprietary software sphere, as proven by this statement, taken directly from
the Unity blog: <I>We�d open source all of Unity today if we thought we could
get away with it and still be in business tomorrow</I>.
</P>

<P>
I don't think that statement makes much sense. Of course taking a more open
approach would require slight adjustments in the way the company does
business, but Unity is already in the market of <I>software as a service</I>.
It's not just the engine itself, its the documentation and updates people
want. Even if they had a fairly open license that allowed reading, modifying
and distributing source code, Unity could keep on raking in the money from the
5% they charge from people who make games built with their engine. The source
being open would, I believe, really have no negative effects from this point
of view.  Also, knowing Unity's limitations (which are many), I'm fairly sure
there's no such advanced technology inside the engine that it would do much
for a competitor to read the code, especially considering their main
competitor, Unreal Engine 4, already has it's source visible.
</P>

<P>
Well, here's to hoping software vendors slowly come to their senses. Free/open
source is not only ethical, but also practical - knowing your underlying
technology from the inside out certainly makes it easier to make a good game.
</P>
[end_content]