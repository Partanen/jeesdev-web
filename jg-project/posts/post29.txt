id = 29
date = 23-11-2018 7:21:24
title = Buy nothing day
author = Kuvis

[begin_content]

<P>
It's that time of the year again: the year's biggest celebration of
consumerism, Christmas, is looming over. But even here in Finland it's not
just Christmas anymore that makes people go and mindlessly buy things they
don't need; no, we need to start the wasteful buying of things early before
December. The American phenomenon of Black Friday has made it's way over here,
machined by the merchants who know well how to exploit the generations that
grew up on the internet, learning everything about all the cool American
"traditions".
</P>

<P>
If you're about to take part in the consumption mania of today, I would ask
you to consider the following: make it a <A
HREF="https://en.wikipedia.org/wiki/Buy_Nothing_Day">buy nothing day</A>
instead. At the very least, ask yourself the following questions.
</P>
<UL>
<LI>Do you really need the thing you thought of buying today? Would you buy
it even if it wasn't on sale? If you have an item similar to what you're
about to buy, still in good condition, do you really need a new one this
soon?</LI>
<LI>Consider what you might do with the money saved if you didn't spontaneously
spend it on things just because they were on sale for one day (or in the case
of Finland, for a whole week!)</LI>
<LI>Think about what the impact of overconsuming things such as electronics is
to the environment.</LI>
</UL>
</P>

<P>
Happy buy nothing day.
</P>
[end_content]