id = 25
date = 21-9-2018 21:46:40
title = Going to India Game Summit 2018
author = Kuvis

[begin_content]

<P>
As I wrote last month, I travelled to Andhra Pradesh, India during the summer
to teach game programming on a summer course over at a university there. At
the end of this September (less than a week from now) I have the pleasure of
going back to the country, this time as an organizer for the first ever <A
HREF="https://indiagamesummit.in/">India Game Summit</A>.
</P>
<P>
The event this year will be a game jam, held at four different universities
and colleges across the country simultaneously. How ever, it is in the hopes
of organizers (the main one of whom is Kajaani University of Applied Sciences)
that the event will grow into a games industry conference in the future,
something akin to the <A HREF="http://www.northerngamesummit.org/">Northern
Game Summit</A> we have in Finland, with speakers from different companies and
organizations sharing their knowledge.
</P>
<P>
An interesting point about organizing a game jam event in India is that it
seems to be a fairly foreign concept, at least at universities (including the
capital area). Even the term <I>game jam</I> has been mostly unheard of;
hence, we've dubbed the event a <I>hackathon</I> instead - a term familiar to
most IT people. Organizers have also been contacted by to-be participants
worried if it is even possible to build a game in 2 days. It may seem funny to
a regular game jammer, but then again there are monetary prizes on the line
here.
</P>
<P>
The event will begin on 28th September and end on the 30th of the same month.
I myself will be travelling to Aditya Engineering College in Andhra Pradesh.
I'm quite excited and interested to see how things work out, to be honest.
</P>

<H3>New website logo</H3>
<P>
In other news, this website has a fancy new logo. Our friend Laura made it. Big
thanks to her!
</P>
[end_content]