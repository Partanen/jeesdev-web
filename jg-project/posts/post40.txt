id = 40
date = 28-9-2019 21:35:8
title = Northern Game Summit 2019
author = Kuvis

[begin_content]

<P>
The yearly <A HREF="http://northerngamesummit.org/">Northern Game Summit</A>
was once again held last Thursday here in Kajaani. It was a fun time as
always.
</P>

<P>
This year the event was held at a local night club rather than the
traditional Biorex movie theatre. I heard some complains about this
arrangement, but I think it turned out quite alright. Maybe a minor part of the
feeling of the grandness of the event was lost, but then again, NGS is
intended to provide a chance for developers to connect with other developers,
and students to connect with them in turn. The night club setting worked, I
think, quite well for that. In terms of practical complaints, the only one
I heard after the event was that the speeches were difficult to follow due to
the layout of the venue, and the placement of the screens.
</P>

<P>
The 2019 speaker list was special in that I'm pretty sure all of the speakers
had some previous connection to Kajaani, many having worked at the local
university of applied sciences or a company located in town. It was cool: as
every year so far, I was disappointed in the low amount of technical topics,
but the other speeches were still useful, some especially to students who are
the most numerous target audience of NGS.
</P>

<P>
I've heard rumours of NGS struggling a bit financially. From the bottom of my
heart, I hope <A HREF="https://www.kamk.fi">KAMK</A> and the other sponsors
keep on supporting the event, of course alongside with the many volunteers
(whom I am very thankful towards despite not volunteering myself).
Kajaani has managed to build a significant game development community
considering the city's population and location, not in small part due to the
game development programmes offered by the local UAS. Some people have worked
hard to achieve this state, with many active companies in the area and
new eager developers graduating every year. Northern Game Summit has an
important place in keeping Kajaani a friendly town for devs, and it deserves
to be kept well and alive.
</P>
[end_content]
