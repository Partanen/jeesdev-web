/* =============================================================================
 * This file is part of Kuvis' Hashtable.
 *
 * Kuvis' Hashtable is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * Kuvis' Hashtable is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Kuvis' Hashtable.  If not, see <https://www.gnu.org/licenses/>.
 * ===========================================================================*/

#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <stdint.h>
#include <stddef.h>

#define HASHTABLE_BUCKET_SIZE 4

/* =============================================================================
 * hashtable()
 * Declare a new hashtable variable. For example:
 *
 * hashtable(size_t, void*) my_table;
 *
 * The above declares a hashtable whose key type is size_t and value type is
 * void *.
 * ===========================================================================*/
#define hashtable(key_type, value_type) \
    struct { \
        struct { \
            struct { \
                key_type    key; \
                value_type  value; \
                size_t      hash; \
                uint8_t     reserved; \
            } items[HASHTABLE_BUCKET_SIZE]; \
        } *buckets; \
        size_t num_buckets; \
        size_t num_values; \
    }

/* =============================================================================
 * hashtable_init()
 * Initialize a hashtable to the number (num_) of initial buckets wanted. If
 * called on a previously initialized hashtable that has not been destroyed,
 * behaviour is undefined.
 * If ret_err_ is not null but instead points to an int *, a return code will be
 * written to it. It will be 0 on success, non-zero otherwise.
 * ===========================================================================*/
#define hashtable_init(table_, num_, ret_err_) \
    (table_.buckets = _hashtable_init(&table_.num_buckets, (num_), \
        sizeof(table_.buckets[0]), &table_.num_values, (ret_err_)))

/* =============================================================================
 * hashtable_destroy()
 * Free the resources used by a hashtable. free_key_func_ may be null, provide
 * it if keys need to be cleaned up.
 * ===========================================================================*/
#define hashtable_destroy(table_, free_key_func_) \
    _hashtable_destroy(&table_, sizeof(table_), (uint8_t*)table_.buckets, \
        free_key_func_, table_.num_buckets, sizeof(table_.buckets[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0], \
            &table_.buckets[0]), sizeof(table_.buckets[0].items[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0].reserved, \
            &table_.buckets[0].items[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0].key, \
            &table_.buckets[0].items[0]))

/* =============================================================================
 * hashtable_insert()
 * Insert a key-value pair into the table. The pair must be provided via a
 * struct that has three members of the correct types named as 'key', 'value'
 * and 'hash'. Example:
 *
 * size_t hash_u32(uint32_t key);
 * hashtable(uint32_t, void *) my_table;
 * ...
 * struct {
 *     uint32_t    key;
 *     void        *value;
 *     size_t      hash;
 * } pair = {324, NULL, hash_u32(324)};
 * hashtable_insert(my_table, pair, 0);
 * ===========================================================================*/
#define hashtable_insert(table_, pair_, ret_err_) \
    hashtable_insert_ext(table_, pair_, hashtable_compare_keys, \
        hashtable_copy_key, ret_err_)

/* =============================================================================
 * hashtable_insert_ext()
 * Similar to hashtable_insert(), but uses custom key comparison and key
 * duplication functions.
 *
 * The provided key comparison function must have the following signature, and
 * it must return 0 if the keys are equal, non-zero otherwise:
 *
 * int compare_custom_keys(const void *a, const void *b, size_t size);
 *
 * The copy function must have the following signature, and it should return 0
 * on success:
 *
 * int copy_custom_key(void *dst, const void *src, size_t size);
 *
 * Defaults are provided for both functions: hashtable_compare_keys() and
 * hashtable_copy_key(). Pass these for functionality identical to
 * hashtable_insert():
 * ===========================================================================*/
#define hashtable_insert_ext(table_, pair_, compare_keys_func_, \
    copy_key_func_, ret_err_) \
    (table_.buckets = _hashtable_insert((ret_err_), \
        (uint8_t*)table_.buckets, &table_.num_buckets, &table_.num_values, \
        sizeof(*table_.buckets), sizeof(table_.buckets[0].items[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0], \
            &table_.buckets[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0].key, \
            &table_.buckets[0].items[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0].value, \
            &table_.buckets[0].items[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0].hash, \
            &table_.buckets[0].items[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0].reserved, \
            &table_.buckets[0].items[0]), \
        &pair_.key, sizeof(pair_.key), pair_.hash, &pair_.value, \
        sizeof(pair_.value), compare_keys_func_, copy_key_func_))

/* =============================================================================
 * hashtable_find()
 * Find a value by key in the given hashtable. On success, the return value is a
 * void pointer to a value. If no value is found, null is returned. Note that
 * the key must be provided to this macro as a pointer.
 * Example:
 *
 * size_t hash_u32(uint32_t key);
 * struct {
 *     uint32_t    key;
 *     size_t      hash;
 * } key_hash_combo = {324, hash_u32(324)};
 * void **value = hashtable_find(my_table, key_hash_combo);
 * if (value)
 *     printf("Value not found\n");
 * else
 *     printf("Value found\n");
 * ===========================================================================*/
#define hashtable_find(table_, key_hash_combo_) \
    hashtable_find_ext(table_, key_hash_combo_, hashtable_compare_keys)

/* =============================================================================
 * hashtable_find_ext()
 * Similar to hashtable_find(), but uses a custom key comparison function. The
 * provided comparison function must have the following signature, and it must
 * return 0 if the keys are equal:
 *
 * int compare_keys(const void *a, const void *b, size_t size);
 * ===========================================================================*/
#define hashtable_find_ext(table_, key_hash_combo_, compare_keys_func_) \
    _hashtable_find(&key_hash_combo_.key, sizeof(key_hash_combo_.key), \
        key_hash_combo_.hash, (uint8_t*)table_.buckets, table_.num_buckets, \
        sizeof(table_.buckets[0]), sizeof(table_.buckets[0].items[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0], \
            &table_.buckets[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0].key, \
            &table_.buckets[0].items[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0].value, \
            &table_.buckets[0].items[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0].reserved, \
            &table_.buckets[0].items[0]), \
        compare_keys_func_)

/* =============================================================================
 * hashtable_erase()
 * Erase the key-value pair of the given key from a hashtable.
 * Example:
 *
 * size_t hash_u32(uint32_t key);
 * hashtable(uint32_t, int) my_table;
 * ...
 * struct {
 *     uint32_t key;
 *     size_t   hash;
 * } key_hash_combo = {324, hash_u32(324)};
 * hashtable_erase(my_table, key_hash_combo_);
 * ===========================================================================*/
#define hashtable_erase(table_, key_hash_combo_) \
    hashtable_erase_ext(table_, key_hash_combo_, hashtable_compare_keys, 0)

/* =============================================================================
 * hashtable_erase_ext()
 * Used similarly to hashtable_erase(), but takes in two extra parameters:
 * a key comparison function, and a free key function. The free key function is
 * allowed to be null if no cleanup for a key is needed.
 * ===========================================================================*/
#define hashtable_erase_ext(table_, key_hash_combo_, compare_keys_func_, \
    free_key_func_) \
    _hashtable_erase((uint8_t*)table_.buckets, table_.num_buckets, \
        &table_.num_values, &key_hash_combo_.key, sizeof(key_hash_combo_.key), \
        key_hash_combo_.hash, \
        sizeof(table_.buckets[0]), sizeof(table_.buckets[0].items[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0], &table_.buckets[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0].key, \
            &table_.buckets[0].items[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0].value, \
            &table_.buckets[0].items[0]), \
        _hashtable_ptr_offset(&table_.buckets[0].items[0].reserved, \
            &table_.buckets[0].items[0]), \
        compare_keys_func_, free_key_func_)

/* =============================================================================
 * hashtable_hash()
 * A default hash function. Uses the 32 bit or 64 bit fnv-a1 algorithm depending
 * on architecture.
 * ===========================================================================*/
size_t hashtable_hash(const void *key, size_t size);

/* =============================================================================
 * hashtable_compare_keys()
 * The default key comparison function. Compares values like memcmp() does.
 * ===========================================================================*/
int hashtable_compare_keys(const void *a, const void *b, size_t size);

/* =============================================================================
 * hashtable_copy_key()
 * The default key copying function. Works similarly to memcpy().
 * ===========================================================================*/
int hashtable_copy_key(void *dst, const void *src, size_t size);

#define _hashtable_ptr_offset(ptr, base) \
    ((size_t)((uint8_t*)(ptr) - (uint8_t*)(base)))

void *_hashtable_init(size_t *num_buckets, size_t num,
    size_t bucket_size, size_t *num_values, int *ret_err);

void _hashtable_destroy(void *table, size_t table_size, uint8_t *buckets,
    void (*free_key)(void *), size_t num_buckets, size_t bucket_size,
    size_t items_off, size_t item_size, size_t reserved_off, size_t key_off);

void *_hashtable_insert(int *ret_err, uint8_t *buckets, size_t *num_buckets,
    size_t *num_values, size_t bucket_size, size_t item_size,
    size_t items_offset, size_t key_off1, size_t value_off1, size_t hash_off1,
    size_t reserved_off1, void *key, size_t key_size, size_t hash, void *value,
    size_t value_size,
    int (*compare_keys)(const void *a, const void *b, size_t size),
    int (*copy_key)(void *dst, const void *src, size_t size));

void *_hashtable_find(void *key, size_t key_size, size_t hash,
    uint8_t *buckets, size_t num_buckets, size_t bucket_size, size_t item_size,
    size_t items_offset, size_t key_offset, size_t value_offset,
    size_t reserved_offset,
    int (*compare_func)(const void *a, const void *b, size_t size));

void _hashtable_erase(uint8_t *buckets, size_t num_buckets, size_t *num_values,
    void *key, size_t key_size, size_t hash, size_t bucket_size,
    size_t item_size, size_t items_off, size_t key_off, size_t value_off,
    size_t reserved_off,
    int (*compare_keys)(const void *a, const void *b, size_t size),
    void (*free_key)(void *key));

#endif
