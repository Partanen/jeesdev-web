#include <math.h>
#include "gui.h"
#include "render.h"
#include "bitmap.h"
#include "../../shared/common_utils.h" /* For gui_assert(), darr funcs */

#if _MUTA_DEBUG
    #define GUI_DEBUG 1
#endif

#define GUI_INPUT_INDICATOR_LIMIT   16
#define GUI_MAX_WINS                512
#define GUI_MAX_GUIDES              512

#if GUI_DEBUG
#   define gui_debug_puts(str)          puts(str)
#   define gui_debug_printf(str, ...)   printf((str), ##__VA_ARGS__)
#else
#   define gui_debug_puts(str)          ((void)0)
#   define gui_debug_printf(str, ...,)  ((void)0)
#endif

typedef struct next_gui_win_t       next_gui_win_t;
typedef struct next_gui_button_t    next_gui_button_t;
typedef struct gui_draw_con_t       gui_draw_con_t;
typedef struct gui_state_t          gui_state_t;
typedef struct gui_win_t            gui_win_t;
typedef struct gui_draw_cmd_t       gui_draw_cmd_darr_t;

struct next_gui_win_t
{
    int     index;
    uint32  last_active;
};

struct next_gui_button_t
{
    uint32  id;
    uint32  parent_id;
    uint32  last_active;
    uint32  mouse_btn;
};

/* Made a separate structure for these variables because was going to use it
 * for the "GUI parent", too (but so far ended up not doing so) */
struct gui_draw_con_t
{
    gui_draw_cmd_darr_t     *draw_cmds;
    float_darr_t            *vert_floats;
};

struct gui_win_t
{
    uint32                  id;
    uint32                  last_hovered;
    uint32                  last_active;
    uint32                  last_drawn;
    int                     dim[4];
    gui_win_state_style_t   *style; /* For child element positioning. Must be
                                     * set by the function that renders the
                                     * window. */
    int                     child_list_index;
    int                     parent_index;
    gui_draw_con_t          draw_con;
    uint8                   flags;
};

struct gui_state_t
{
    bool32                  began_update;
    bool32                  input_indctr_active;
    uint                    frame_count;
    enum gui_origin_t       origin;
    int                     coord_space[4];
    sfont_t                 *font;
    uint8                   text_color[4];
    uint8                   input_indctr_timer;
    int                     active_text_input;

    /* "Last" states for different gui object types */
    int                     last_text_origin;
    int                     last_text_rect[4];
    int                     last_button_origin;
    int                     last_button_rect[4];
    int                     last_texture_origin;
    int                     last_texture_rect[4];

    /* Allocated separately just because of the possible realloc
     * complications */
    char                    *fmt_buf;
    int                     fmt_buf_size;

    gui_win_t               *wins;
    int                     win_num;
    int                     cur_win;
    int                     *wins_open_parents;
    int                     wins_open_parents_num;
    int                     wins_open_num;
    int                     *win_stack;
    int                     win_stack_num;

    int                     *guides; /* Array of int arrays of 4 slots */
    int                     guides_num;

    int_darr_t              **win_child_lists;

    /* Active window */
    uint32                  active_win_id;
    uint32                  hovered_win_id;
    next_gui_win_t          next_act_win;
    next_gui_win_t          next_hov_win;

    /* Buttons */
    next_gui_button_t       next_prs_btn;
    next_gui_button_t       next_hov_btn;
    int                     hovered_button;
    uint32                  pressed_button_id, pressed_button_pid;
    uint32                  pressed_button_mbtn;
    uint32                  released_button_id;

    /* Input state */
    gui_input_state_t       input_state;
    gui_input_state_t       last_input_state;

    uint32                  mbtns_down_now;
    uint32                  mbtns_up_now;

    int                     clicked_this_frame;
    int                     released_this_frame;
    int                     was_pressed_last_frame;

    /* Bitmask of the mouse buttons which have been used to press some gui
     * element */
    uint32                  pressed_element_button_flags;

    int                     coord_inside_win_x, coord_inside_win_y;

    /* Callbacks */
    void                    (*update_inputs)(gui_input_state_t *input_state);
    void                    (*render)(gui_draw_list_t *lists, int num_lists);

    /* Draw lists:
     * There's always an equal number of draw lists to the number of windows */
    gui_draw_list_t         *draw_lists;

    gui_button_style_t      *button_style;
    gui_win_style_t         *win_style;
    gui_tex_button_style_t  *tex_button_style;
    gui_progressbar_style_t *progressbar_style;
};

gui_win_t       _wins[GUI_MAX_WINS];
int             _guides[GUI_MAX_GUIDES * 4];
int             _win_stack[GUI_MAX_WINS];
int             _wins_open_parents[GUI_MAX_WINS];
gui_draw_list_t _draw_lists[GUI_MAX_WINS];
int_darr_t      *_win_child_lists[GUI_MAX_WINS];

#define gui_malloc  malloc
#define gui_calloc  calloc
#define gui_realloc realloc
#define gui_free    free
#define gui_assert  muta_assert

#define GET_WIN_INDEX(win_ptr) ((int)((size_t)((win_ptr) - gui.wins)))
#define GUI_WIN_STACK_TOP() \
    (gui.win_stack_num > 0 ? (&gui.wins[gui.win_stack[gui.win_stack_num - 1]]) \
        : 0)
#define GUIDE_STACK_TOP() \
    (gui.guides_num > 0 ? (&gui.guides[gui.guides_num - 1]) : 0)
#define GUI_COMPUTE_WIN_CONTENT_RECT(win, ret_int_arr) \
    if ((win)->style) \
    { \
        (ret_int_arr)[0] = (win)->dim[0] + (win)->style->bw_l; \
        (ret_int_arr)[1] = (win)->dim[1] + (win)->style->bw_t; \
        (ret_int_arr)[2] = (win)->dim[2] - (win)->style->bw_l - \
            (win)->style->bw_r; \
        (ret_int_arr)[3] = (win)->dim[3] - (win)->style->bw_t - \
            (win)->style->bw_t; \
    } else \
    { \
        for (int i = 0; i < 4; ++i) \
            (ret_int_arr)[i] = (win)->dim[i]; \
    }
#define GUI_WIN_CHILD_LIST(win_ptr) \
    ((win_ptr)->child_list_index >= 0 ? \
    &gui.win_child_lists[(win_ptr)->child_list_index] : 0)
#define WIN_CHILDREN(win_ptr) (GUI_WIN_CHILD_LIST(win_ptr)->indices)
#define GUI_TEST_MOUSE_RECT(x, y, w, h) \
    (gui.input_state.mx >= (x) && gui.input_state.mx <= ((x) + (w)) && \
     gui.input_state.my >= (y) && gui.input_state.my <= ((y) + (h)))
#define GUI_SET_COLOR(col, r, g, b, a) \
    ((col)[0] = (r), (col)[1] = (g), (col)[2] = (b), (col)[3] = (a))
#define GUI_COPY_COLOR(dst, src) \
    (dst)[0] = (src)[0]; (dst)[1] = (src)[1]; \
    (dst)[2] = (src)[2]; (dst)[3] = (src)[3];
#define GUI_GET_FLAG(val, flag) (((val) & (flag)) == (flag) ? 1 : 0)
#define GUI_SET_FLAG_ON(val, flag)   ((val) |= (flag))
#define GUI_SET_FLAG_OFF(val, flag)  ((val) &= ~(flag))
#define GUI_MB_DOWN(b) (GUI_GET_FLAG(gui.input_state.buttons, (b)))
#define GUI_MB_DOWN_NOW(b) \
    (GUI_GET_FLAG(gui.input_state.buttons, (b)) && \
    !GUI_GET_FLAG(gui.last_input_state.buttons, (b)))
#define GUI_ONE_OF_BUTTONS_DOWN_NOW(mask) \
    (mask & gui.input_state.buttons)
#define GUI_MB_UP_NOW(b) \
    (GUI_GET_FLAG(gui.last_input_state.buttons, (b)) && \
    !GUI_GET_FLAG(gui.input_state.buttons, (b)))
#define GUI_COMPUTE_QUAD_XY(x_ptr, y_ptr, origin, x, y, w, h) \
{ \
    int dim_[4]; \
    int *guide_ = GUIDE_STACK_TOP(); \
\
    if (guide_) \
        memcpy(dim_, guide_, 4 * sizeof(int)); \
    else if (gui.win_stack_num > 0) \
    { \
        gui_win_t *p = GUI_WIN_STACK_TOP(); \
        for (int i = 0; i < 4; ++i) dim_[i] = p->dim[i]; \
        if (p->style) \
        { \
            dim_[0] += p->style->bw_l; \
            dim_[1] += p->style->bw_t; \
            dim_[2] = dim_[2] - p->style->bw_l - p->style->bw_r; \
            dim_[3] = dim_[3] - p->style->bw_t - p->style->bw_b; \
        } \
    } else \
        {for (int i = 0; i < 4; ++i) dim_[i] = gui.coord_space[i];} \
\
    _gui_compute_quad_parented_xy((x_ptr), (y_ptr), origin, dim_, x, y, w, h); \
}
#define GUI_WRITE_TEX_QUAD_VERTS(verts, tex, clip, x, y, w, h, col) \
    verts[ 0] = x; \
    verts[ 1] = y; \
    verts[ 5] = x + w; \
    verts[ 6] = y; \
    verts[10] = x; \
    verts[11] = y + h; \
    verts[15] = x + w; \
    verts[16] = y + h; \
    verts[ 2] = clip[0] / tex->w; \
    verts[ 3] = clip[1] / tex->h; \
    verts[ 7] = clip[2] / tex->w; \
    verts[ 8] = clip[1] / tex->h; \
    verts[12] = clip[0] / tex->w; \
    verts[13] = clip[3] / tex->h; \
    verts[17] = clip[2] / tex->w; \
    verts[18] = clip[3] / tex->h; \
    uint8* bverts_ = (uint8*)verts; \
    bverts_[16] = col[0]; \
    bverts_[17] = col[1]; \
    bverts_[18] = col[2]; \
    bverts_[19] = col[3]; \
    bverts_[36] = col[0]; \
    bverts_[37] = col[1]; \
    bverts_[38] = col[2]; \
    bverts_[39] = col[3]; \
    bverts_[56] = col[0]; \
    bverts_[57] = col[1]; \
    bverts_[58] = col[2]; \
    bverts_[59] = col[3]; \
    bverts_[76] = col[0]; \
    bverts_[77] = col[1]; \
    bverts_[78] = col[2]; \
    bverts_[79] = col[3];
#define GUI_WRITE_FLIPPED_TEX_QUAD_VERTS(verts, tex, clip, x, y, w, h, col, \
    flip) \
{ \
    switch(flip) \
    { \
        default: \
        case GUI_FLIP_NONE: \
            GUI_WRITE_TEX_QUAD_VERTS(verts, tex, clip, x, y, w, h, col); \
            return; \
        case GUI_FLIP_H: \
            verts[ 0] = x + w; \
            verts[ 1] = y; \
            verts[ 5] = x; \
            verts[ 6] = y; \
            verts[10] = x + w; \
            verts[11] = y + h; \
            verts[15] = x; \
            verts[16] = y + h; \
            break; \
        case GUI_FLIP_V: \
            verts[ 0] = x; \
            verts[ 1] = y + h; \
            verts[ 5] = x + w; \
            verts[ 6] = y + h; \
            verts[10] = x; \
            verts[11] = y; \
            verts[15] = x + w; \
            verts[16] = y; \
            break; \
        case GUI_FLIP_BOTH: \
            verts[ 0] = x + w; \
            verts[ 1] = y + h; \
            verts[ 5] = x; \
            verts[ 6] = y + h; \
            verts[10] = x + w; \
            verts[11] = y; \
            verts[15] = x; \
            verts[16] = y; \
            break; \
    } \
    verts[ 2] = clip[0] / tex->w; \
    verts[ 3] = clip[1] / tex->h; \
    verts[ 7] = clip[2] / tex->w; \
    verts[ 8] = clip[1] / tex->h; \
    verts[12] = clip[0] / tex->w; \
    verts[13] = clip[3] / tex->h; \
    verts[17] = clip[2] / tex->w; \
    verts[18] = clip[3] / tex->h; \
    uint8* bverts_ = (uint8*)verts; \
    bverts_[16] = col[0]; \
    bverts_[17] = col[1]; \
    bverts_[18] = col[2]; \
    bverts_[19] = col[3]; \
    bverts_[36] = col[0]; \
    bverts_[37] = col[1]; \
    bverts_[38] = col[2]; \
    bverts_[39] = col[3]; \
    bverts_[56] = col[0]; \
    bverts_[57] = col[1]; \
    bverts_[58] = col[2]; \
    bverts_[59] = col[3]; \
    bverts_[76] = col[0]; \
    bverts_[77] = col[1]; \
    bverts_[78] = col[2]; \
    bverts_[79] = col[3]; \
}
#define GUI_ROTATE_QUAD(verts, x, y, w, h, rot) \
{ \
    if (rot)    \
    { \
        float tx, ty, rx, ry; \
        float ox = (w / 2.0f) + x; \
        float oy = (h / 2.0f) + y; \
        for (int i = 0; i < 4; ++i) \
        { \
            tx = verts[(i * 5) + 0] - ox; \
            ty = verts[(i * 5) + 1] - oy; \
            rx = tx * cosf(rot) - ty * sinf(rot); \
            ry = tx * sinf(rot) + ty * cosf(rot); \
            verts[(i * 5) + 0] = rx + ox; \
            verts[(i * 5) + 1] = ry + oy; \
        } \
    } \
}

static gui_state_t              gui                 = {0};
static uint8                    gui_color_white[4]  = {255, 255, 255, 255};
static uint8                    gui_color_blue[4]   = { 10,  62, 179, 255};
static uint8                    gui_color_black[4]  = {  0,   0,   0, 255};
static gui_win_style_t          df_win_style;
static gui_button_style_t       df_btn_style;
static gui_tex_button_style_t   df_tex_btn_style;
static gui_progressbar_style_t  df_progressbar_style;
static gui_button_style_t       invisible_btn_style;
static const char               *_gui_base_win_name = "__GUI_BASE_WIN__";
static uint32                   _gui_base_win_id;

//TODO: (JL: get_progress bar id. progress bareille id?)

static uint32
_gui_graphic_button(uint32 id, const char *title, int x, int y, int w, int h, uint32 mbtn_mask);

static uint32
_gui_graphic_tex_button(uint32 id, const char *title, int x, int y, int w, int h, uint32 mbtn_mask);

static gui_win_t *
_gui_get_win_by_id(uint32 id);

static gui_win_t *
_gui_create_win(uint32 id);

static void
_gui_compute_quad_parented_xy(int *ret_x, int *ret_y, enum gui_origin_t o,
    int *parent_dims, int x,  int y,  int w,  int h);

static gui_win_t *
_gui_begin_win_internal(const char *title, int x, int y, int w, int h, int flags);

static void
gui_end_win_internal();

static int
_gui_win_draw_quad(gui_win_t *win, int x, int y, int w, int h, tex_t *tex,
    float *clip, uint8 *color, enum gui_flip_t flip, float rot);

static int
_gui_win_draw_quad_f(gui_win_t *win, float x, float y, float w, float h, tex_t *tex,
    float *clip, uint8 *color, enum gui_flip_t flip, float rot);

static int
_gui_draw_con_draw_text(gui_draw_con_t *con, const char *txt, sfont_t *sf,
    float x, float y, int *parent_dim, int origin, int wrap,
    float sx, float sy, uint8 *col, int *ret_rect);

static int
_gui_draw_con_draw_title_text(gui_draw_con_t *con, const char *txt, sfont_t *sf,
    int x, int y, int *parent_dim, int origin, float sx, float sy,
    uint8 *col);

static void
_gui_write_quad_verts(float *verts, tex_t *tex, float *clip, float x, float y,
    float w, float h, uint8 *col, enum gui_flip_t flip);

/* Note: assumes children actually exist */
static void
_gui_sort_win_children(gui_win_t *win);

static void
_gui_add_win_to_draw_list(gui_win_t *win, gui_win_t *all_wins,
    gui_draw_list_t *lists, int *num_lists);

static inline bool32
_gui_test_next_active_win_parenthood();

static inline bool32
_gui_test_next_hovered_win_parenthood();

static int
_gui_draw_con_init(gui_draw_con_t *con, int num_cmds);

static inline void
_gui_draw_con_clear(gui_draw_con_t *con);

static float *
_gui_draw_con_reserve_verts(gui_draw_con_t *con, int num_verts);

static int
_gui_draw_con_push_draw_cmd(gui_draw_con_t *con, int num_verts,
    tex_t *tex);

static int
_gui_compute_title_text_line_w(const char *txt, sfont_t *f, float x_scale);
/* Ignores double ## as they mark the ending of a visible title */

static void
_gui_compute_text_block_wh(const char *txt, sfont_t *f, float wrap, float sx,
    float sy, int *ret_w, int *ret_h);

static inline int
_gui_strlen(const char *txt);
/* For calculating the number of quads required for a drawable string */

static inline int
_gui_title_strlen(const char *txt);
/* For calculating the number of quads required for a drawable title string */

static int
_gui_realloc_fmt_buf(int new_size);

static uint32
_gui_button_internal(uint32 id, int x, int y, int w, int h,
    uint32 mbtn_mask, int *ret_tx, int *ret_ty, gui_win_t **ret_parent);

static int
_gui_claim_win_child_list();
/* Returns < 0 on failure */

int
gui_init(int max_wins, void (*update_inputs)(gui_input_state_t*),
    void (*render)(gui_draw_list_t*, int))
{
    if (!update_inputs || !render)
        return 1;

    gui.wins                = _wins;
    gui.guides              = _guides;
    gui.win_stack           = _win_stack;
    gui.wins_open_parents   = _wins_open_parents;
    gui.draw_lists          = _draw_lists;
    gui.win_child_lists     = _win_child_lists;

    int ret = 0;

    /* Format buffer */
    gui.fmt_buf         = gui_malloc(1024);
    gui.fmt_buf_size    = 1024;

    if (!gui.fmt_buf)
        {ret = 7; goto out;}

    gui.update_inputs           = update_inputs;
    gui.render                  = render;
    gui.win_num                 = 0;
    gui.frame_count             = 0;
    gui.began_update            = 0;
    gui.input_indctr_active     = 0;
    gui.input_indctr_timer      = 0;
    gui.hovered_win_id          = 0;
    gui.active_win_id           = 0;
    gui.coord_space[0]          = 0;
    gui.coord_space[1]          = 0;
    gui.pressed_button_id       = 0;
    gui.hovered_button          = 0;
    gui.active_text_input       = 0;
    gui.button_style            = &df_btn_style;
    gui.win_style               = &df_win_style;
    gui.tex_button_style        = &df_tex_btn_style;
    gui.progressbar_style       = &df_progressbar_style;
    gui.font                    = 0;

    df_win_style                = gui_create_win_style();
    df_btn_style                = gui_create_button_style();
    df_tex_btn_style            = gui_create_tex_button_style();
    df_progressbar_style        = gui_create_progressbar_style();
    memset(&invisible_btn_style, 0, sizeof(gui_button_style_t));

    _gui_base_win_id = fnv_hash32_from_str(_gui_base_win_name);

    out:
        if (ret)
            gui_destroy(gui);
        return ret;
}

void
gui_destroy()
{
    gui_free(gui.fmt_buf);
    gui.fmt_buf = 0;
    for (int i = 0; i < GUI_MAX_WINS; ++i)
        darr_free(gui.win_child_lists[i]);
    for (int i = 0; i < GUI_MAX_WINS; ++i)
    {
        darr_free(gui.wins[i].draw_con.draw_cmds);
        darr_free(gui.wins[i].draw_con.vert_floats);
    }
    memset(_wins, 0, sizeof(_wins));
    memset(_win_child_lists, 0, sizeof(_win_child_lists));
    memset(&gui, 0, sizeof(gui));
}

void
gui_clear()
    {gui.win_num = 0;}

int
gui_begin()
{
    gui_assert(!gui.began_update);
    gui_assert(gui.win_stack_num == 0);

    gui.next_act_win.index              = -1;
    gui.next_hov_win.index              = -1;
    gui.next_prs_btn.id                 = 0;
    gui.next_hov_btn.id                 = 0;
    gui.win_stack_num                   = 0;
    gui.wins_open_num                   = 0;
    gui.wins_open_parents_num           = 0;
    gui.last_input_state                = gui.input_state;
    gui.began_update                    = 1;
    gui.origin                          = GUI_TOP_LEFT;
    gui.button_style                    = &df_btn_style;
    gui.win_style                       = &df_win_style;
    gui.progressbar_style               = &df_progressbar_style;
    gui.released_button_id              = 0;
    gui.clicked_this_frame              = 0;
    gui.guides_num                      = 0;
    darr_clear(gui.win_child_lists);

    for (int i = 0; i < 4; ++i) gui.last_text_rect[i]       = 0;
    for (int i = 0; i < 4; ++i) gui.last_button_rect[i]     = 0;
    for (int i = 0; i < 4; ++i) gui.last_texture_rect[i]    = 0;
    gui.last_text_origin    = GUI_TOP_LEFT;
    gui.last_button_origin  = GUI_TOP_LEFT;
    gui.last_texture_origin = GUI_TOP_LEFT;

    gui.update_inputs(&gui.input_state);

    gui.mbtns_down_now = gui.input_state.buttons & \
        (gui.input_state.buttons ^ gui.last_input_state.buttons);

    gui.mbtns_up_now = gui.last_input_state.buttons & \
        (gui.last_input_state.buttons ^ gui.input_state.buttons);

    /* Do this after updating inputs */
    if (gui.pressed_element_button_flags && !gui.input_state.buttons)
        gui.released_this_frame = 1;
    else
        gui.released_this_frame = 0;
    /* ---- */

    for (int i = 0; i < 4; ++i)
        gui.coord_space[i] = gui.input_state.coord_space[i];
    gui_text_color(255, 255, 255, 255);
    return gui_begin_empty_win(_gui_base_win_name, 0, 0,
        gui.input_state.coord_space[2], gui.input_state.coord_space[3], 0);
}

int
gui_end()
{
    gui_assert(gui.began_update);
    gui_assert(gui.win_stack_num == 1);
    gui_end_win_internal();
    uint32 next_act_id;

    /* Set the active window */
    gui_win_t *next_act_win = gui.next_act_win.index < 0 ? 0 : \
        &gui.wins[gui.next_act_win.index];

    if (next_act_win)
    {
        next_act_win->last_active   = gui.frame_count;
        next_act_id                 = next_act_win->id;
        gui_win_t *parent;

        /* Also make the window's parents active */
        /* NOTE: Should this happen if the parent is click-through? */
        for (int pi = next_act_win->parent_index;
             pi != -1;
             pi = parent->parent_index)
        {
            parent              = &gui.wins[pi];
            parent->last_active = gui.frame_count;
        }
    } else
        next_act_id = 0;

    if (next_act_id)
        gui.active_win_id = next_act_id;

    /* Set the hovered window */
    if (!gui.input_state.buttons)
    {
        if (gui.next_hov_win.index >= 0)
            gui.hovered_win_id = gui.wins[gui.next_hov_win.index].id;

        /* There can only be a hovered button if no mouse button is currently
         * pressed */
        if (!gui.pressed_button_id)
            gui.hovered_button = gui.next_hov_btn.id;

        gui.pressed_element_button_flags = 0;
    }

    if (!(gui.input_state.buttons && gui.pressed_button_id
        && gui.hovered_win_id == gui.pressed_button_pid))
    {
        gui.pressed_button_id   = gui.next_prs_btn.id;
        gui.pressed_button_pid  = gui.next_prs_btn.parent_id;
        gui.pressed_button_mbtn = gui.next_prs_btn.mouse_btn;
    }

#if 0
    /* Remove inactive windows */
    for (int i = 0; i < gui.win_num; ++i)
    {
        if (gui.wins[i].last_drawn != frame_count)
        {
            if (i != gui.win_num - 1)
                gui.wins[i] = gui.wins[gui.win_num - 1];
            --gui.win_num;
        }
    }
#endif

    /* Quit here if we don't have any active windows anyway */
    if (gui.wins_open_num == 0)
        return 0;

    /* Sort active windows */
    int         *active_parent_wins = gui.wins_open_parents;
    bool32      swapped             = 1;
    gui_win_t   *win, *owin;
    int         tmp;

    while (swapped)
    {
        swapped = 0;
        for (int i = 0; i < gui.wins_open_parents_num - 1; ++i)
        {
            win     = &gui.wins[active_parent_wins[i]];
            owin    = &gui.wins[active_parent_wins[i + 1]];
            if (owin->last_active >= win->last_active)
                continue;
            tmp = active_parent_wins[i];
            active_parent_wins[i]      = active_parent_wins[i + 1];
            active_parent_wins[i + 1]  = tmp;
            swapped = 1;
        }
    }

    /* Build draw lists and a list of lists */
    gui_draw_list_t *lists  = gui.draw_lists;
    int num_lists           = 0;

    int_darr_t **child_list;

    for (int i = 0; i < gui.wins_open_parents_num; ++i)
    {
        win         = &gui.wins[active_parent_wins[i]];
        child_list  = GUI_WIN_CHILD_LIST(win);
        if (child_list && darr_num(*child_list) > 0)
            _gui_sort_win_children(win);
        _gui_add_win_to_draw_list(win, gui.wins, lists, &num_lists);
    }

    gui.input_indctr_timer++;
    if (gui.input_indctr_timer == GUI_INPUT_INDICATOR_LIMIT)
    {
        gui.input_indctr_active = gui.input_indctr_active ? 0 : 1;
        gui.input_indctr_timer = 0;
    }

    gui.render(lists, num_lists);
    gui.began_update = 0;
    gui.frame_count++;

    return 0;
}

bool32
gui_was_clicked()
    {return gui.clicked_this_frame;}

bool32
gui_was_released()
    {return gui.released_this_frame;}

uint32
gui_get_active_win_id()
    {return gui.active_win_id;}

uint32
gui_is_any_element_pressed()
    {return gui.pressed_element_button_flags;}

uint32
gui_triggered_button()
    {return gui.released_button_id;}

int
gui_is_a_button_pressed()
    {return gui.pressed_button_id ? 1 : 0;}

int
gui_cur_win_w()
{
    gui_win_t *p = GUI_WIN_STACK_TOP();
    if (p) return p->dim[2];
    return 0;
}

int
gui_cur_win_h()
{
    gui_win_t *p = GUI_WIN_STACK_TOP();
    if (p)
        return p->dim[3];
    return 0;
}

static gui_win_t *
_gui_begin_win_internal(const char *title, int x, int y, int w, int h,
    int flags)
{
    uint32      id      = fnv_hash32_from_str(title);
    gui_win_t   *win    = _gui_get_win_by_id(id);

    if (!win && !(win = _gui_create_win(id)))
    {
        muta_assert(0);
        return 0;
    }

    GUI_COMPUTE_QUAD_XY(&win->dim[0], &win->dim[1], gui.origin, x, y, w, h);

    win->flags              = (uint8)flags;
    win->dim[2]             = w;
    win->dim[3]             = h;
    win->last_drawn         = gui.frame_count;
    win->child_list_index   = -1;
    _gui_draw_con_clear(&win->draw_con);

    int win_index = GET_WIN_INDEX(win);

    /* Determine if this window is potentially the next hovered or active
     * window */
    if (GUI_TEST_MOUSE_RECT(win->dim[0], win->dim[1], win->dim[2], win->dim[3]))
    {
        /* TODO: changed to any mouse button */
        if ((GUI_MB_DOWN_NOW(GUI_MB_LEFT) || GUI_MB_DOWN_NOW(GUI_MB_RIGHT))
        && !GUI_GET_FLAG(flags, GUI_WIN_CLICKTHROUGH))
        {
            if (gui.next_act_win.index < 0
                || (gui.next_act_win.last_active <= win->last_active
                    || _gui_test_next_active_win_parenthood()))
            {
                gui.next_act_win.index          = win_index;
                gui.next_act_win.last_active    = win->last_active;
                if (id != _gui_base_win_id)
                {
                    gui.clicked_this_frame = 1;
                    gui.pressed_element_button_flags = gui.input_state.buttons;
                    gui.coord_inside_win_x = gui.input_state.mx - win->dim[0];
                    gui.coord_inside_win_y = gui.input_state.my - win->dim[1];
                }
            }
        }

        if (gui.next_hov_win.index < 0
        || (gui.next_hov_win.last_active <= win->last_active
            || _gui_test_next_hovered_win_parenthood()))
        {
            gui.next_hov_win.index          = win_index;
            gui.next_hov_win.last_active    = win->last_active;
        }
    }

    /* If this is a child window, push it to the child array of the parent */
    gui_win_t *p = GUI_WIN_STACK_TOP();

    if (p)
    {
        win->parent_index       = gui.win_stack[gui.win_stack_num - 1];
        int_darr_t **child_list = GUI_WIN_CHILD_LIST(p);
        if (!child_list)
        {
            int list_index = _gui_claim_win_child_list();
            if (list_index < 0)
            {
                muta_assert(0);
                return 0;
            }
            p->child_list_index = list_index;
            child_list = GUI_WIN_CHILD_LIST(p);
        }
        darr_push(*child_list, win_index);
        gui_assert(win_index != GET_WIN_INDEX(p));
    } else /* Only push to open win list if win has no parent */
    {
        win->parent_index = -1;
        gui.wins_open_parents[gui.wins_open_parents_num++] = win_index;
    }

    gui.wins_open_num++;

    /* Push on to the win stack */
    gui.win_stack[gui.win_stack_num] = win_index;
    gui.win_stack_num++;

    gui_assert(win);
    return win;
}

static void
gui_end_win_internal()
{
    gui.win_stack_num--;
    gui.guides_num = 0;
}

uint32
gui_begin_win(const char *title, int x, int y, int w, int h, int flags)
{
    if (!title)
        return 0;

    gui_win_t *win = _gui_begin_win_internal(title, x, y, w, h, flags);
    if (!win)
        return 0;

    uint32 id = win->id;

    /* Render */
    gui_win_state_style_t *ss;

    if (id == gui.active_win_id)
        ss = &gui.win_style->active;
    else if (id == gui.hovered_win_id)
        ss = &gui.win_style->hovered;
    else
        ss = &gui.win_style->inactive;

    win->style = ss;
    int num_rend_errors = 0;

    /* Border */
    num_rend_errors += _gui_win_draw_quad(win, win->dim[0], win->dim[1],
        win->dim[2], win->dim[3], 0, 0, ss->br_col, GUI_FLIP_NONE, 0);

    /* Title */
    sfont_t *sf = gui.font;

    if (sf && sfont_is_loaded(sf))
        num_rend_errors += _gui_draw_con_draw_title_text(&win->draw_con, title,
            gui.font, ss->title_offset[0], ss->title_offset[1], win->dim,
            ss->title_origin, ss->title_scale[0], ss->title_scale[1],
            gui_color_white);

    /* Background */
    num_rend_errors += _gui_win_draw_quad(win,
        win->dim[0] + ss->bw_l,
        win->dim[1] + ss->bw_t,
        win->dim[2] - ss->bw_l- ss->bw_r,
        win->dim[3] - ss->bw_t- ss->bw_b,
        0, 0, ss->bg_col, GUI_FLIP_NONE, 0);

    if (num_rend_errors > 0)
        muta_panic(num_rend_errors, __func__);
    return id;
}

uint32
gui_begin_empty_win(const char *title, int x, int y, int w, int h, int flags)
{
    gui_win_t *win = _gui_begin_win_internal(title, x, y, w, h, flags);
    if (!win)
        return 0;
    win->style = 0;
    return win->id;
}

int
gui_end_win()
{
    gui_assert(gui.win_stack_num > 1);
    if (gui.win_stack_num <= 1)
        return 1;
    gui_end_win_internal();
    return 0;
}

int
gui_begin_guide(int x, int y, int w, int h)
{
    if (gui.guides_num == GUI_MAX_GUIDES)
        return 1;
    int *g = &gui.guides[gui.guides_num * 4];
    GUI_COMPUTE_QUAD_XY(&g[0], &g[1], gui.origin, x, y, w, h);
    g[2] = w; g[3] = h;
    gui.guides_num++;
    return 0;
}

void
gui_end_guide()
{
    gui_assert(gui.guides_num > 0);
    gui.guides_num--;
}

int
gui_progressbar(const char *title, int x, int y, int w, int h)
{
    gui_win_t *p = GUI_WIN_STACK_TOP();
    int tx, ty;

    gui_progressbar_style_t *bs;
    bs = gui.progressbar_style;
    if (!bs)
        return 1;

    GUI_COMPUTE_QUAD_XY(&tx, &ty, gui.origin, x, y, w, h);

    if ((float)w > bs->clip[2]) bs->clip[2] = (float)w;
    if ((float)h > bs->clip[3]) bs->clip[3] = (float)h;
    _gui_win_draw_quad(p, tx, ty, (int)bs->clip[2], (int)bs->clip[3], 0, 0,
        bs->bg_col, GUI_FLIP_NONE, 0);
    _gui_win_draw_quad(p, tx, ty, w, h, 0, 0, bs->fill_col, GUI_FLIP_NONE, 0);

    sfont_t *t_font = bs->font ? bs->font : gui.font;
    if (t_font && sfont_is_loaded(t_font))
    {
        int rect[] = { tx, ty, (int)bs->clip[2], (int)bs->clip[3] };
        _gui_draw_con_draw_title_text(&p->draw_con, title, t_font,
            bs->title_offset[0], bs->title_offset[1], rect, bs->title_origin,
            bs->title_scale[0], bs->title_scale[1], gui_color_white);
    }
    return 0;
}

int
gui_progressbar_tex(const char *title, int x, int y, int w, int h, int max_val)
{
    gui_win_t               *p  = GUI_WIN_STACK_TOP();
    gui_progressbar_style_t *bs = gui.progressbar_style;
    int                     crect[4];
    float                   clip[4];

    if (!bs)
    {
        gui_assert(0);
        return 1;
    }

    if (!bs->tex)
        goto render_title;

    GUI_COMPUTE_WIN_CONTENT_RECT(p, crect);

    for (int i = 0; i < 4; ++i)
        clip[i] = bs->clip[i];

    int sw = (int)(clip[2] - clip[0]);
    int sh = (int)(clip[3] - clip[1]);
    float fw = (float)(((float)w / (float)max_val) * sw);
    float fh = (float)(((float)h / (float)max_val) * sh);
    clip[2] = clip[0] + fw;
    clip[3] = clip[1] + fh;

    int tex_x, tex_y;

    _gui_compute_quad_parented_xy(&tex_x, &tex_y, gui.origin,
        crect, x, y, sw, sh);

    _gui_win_draw_quad(p, tex_x, tex_y, (int)fw, (int)fh,
        bs->tex, clip, gui_color_white, GUI_FLIP_NONE, 0);

    render_title:;
    sfont_t *t_font = bs->font ? bs->font : gui.font;
    if (t_font && sfont_is_loaded(t_font))
    {
        int rect[] = { tex_x, tex_y, sw, sh };
        _gui_draw_con_draw_title_text(&p->draw_con, title, t_font,
            bs->title_offset[0], bs->title_offset[1], rect, bs->title_origin,
            bs->title_scale[0], bs->title_scale[1], gui_color_white);
    }
    return 0;
}

int
gui_progressbar_tex_clipped(const char *title, int x, int y, int w, int h, int gable_w)
{
    gui_win_t   *p = GUI_WIN_STACK_TOP();
    int         crect[4];
    float       maxclip[4];
    float       beginclip[4];
    float       endclip[4];

    gui_progressbar_style_t *bs;
    bs = gui.progressbar_style;

    if (!bs)
    {
        puts("Defaulting &gui.progressbar_style not found! returning 1 at gui_progress_bar\n");
        return 1;
    }
    if (!bs->tex)
    {
        puts("bs->tex not found in gui_progressbar_tex\n");
        goto render_title;
    }

    GUI_COMPUTE_WIN_CONTENT_RECT(p, crect);

    for (int i = 0; i < 4; ++i)
    {
        beginclip[i] = bs->clip[i];
        endclip[i]   = bs->clip[i];
        maxclip[i]   = bs->clip[i];
    }

    beginclip[2] = maxclip[0] + gable_w;
    endclip[0]   = maxclip[2] - gable_w;

    int sw = (int)(maxclip[2] - maxclip[0]);
    int sh = (int)(maxclip[3] - maxclip[1]);

    float fw = (maxclip[2] / 100) * w;
    float fh = (maxclip[3] / 100) * h;

    int tex_x, tex_y;

    _gui_compute_quad_parented_xy(&tex_x, &tex_y, gui.origin,
        crect, x, y, sw, sh);
    _gui_win_draw_quad_f(p, (float)tex_x, (float)tex_y, fw, fh,
        bs->tex, maxclip, gui_color_white, GUI_FLIP_NONE, 0);
    _gui_win_draw_quad_f(p, (float)tex_x - gable_w, (float)tex_y, (float)gable_w, fh,
        bs->tex, beginclip, gui_color_white, GUI_FLIP_NONE, 0);
    _gui_win_draw_quad_f(p, (float)tex_x + fw, (float)tex_y, (float)gable_w, fh,
        bs->tex, endclip, gui_color_white, GUI_FLIP_NONE, 0);

    render_title:;
    sfont_t *t_font = bs->font ? bs->font : gui.font;
    if (t_font && sfont_is_loaded(t_font))
    {
        int rect[] = { tex_x, tex_y, sw, sh };
        _gui_draw_con_draw_title_text(&p->draw_con, title, t_font,
            bs->title_offset[0], bs->title_offset[1], rect, bs->title_origin,
            bs->title_scale[0], bs->title_scale[1], gui_color_white);
    }

    return 0;
}

uint32
gui_repeat_button(const char *title, int x, int y, int w, int h, uint32 mbtn_mask)
{
    gui_assert(title);
    uint32 id = fnv_hash32_from_str(title);
    _gui_graphic_button(id, title, x, y, w, h, mbtn_mask);
    if (gui.pressed_button_id == id) return gui.pressed_button_mbtn;
    return 0;
}

uint32
gui_repeat_tex_button(const char *title, int x, int y, int w, int h, uint32 mbtn_mask)
{
    gui_assert(title);
    uint32 id = fnv_hash32_from_str(title);
    _gui_graphic_tex_button(id, title, x, y, w, h, mbtn_mask);
    if (gui.pressed_button_id == id) return gui.pressed_button_mbtn;
    return 0;
}

uint32
gui_repeat_invisible_button(const char *title, int x, int y, int w, int h, uint32 mbtn_mask)
{
    gui_assert(title);
    uint32 id = fnv_hash32_from_str(title);
    _gui_button_internal(id, x, y, w, h, mbtn_mask, 0, 0, 0);
    if (gui.pressed_button_id == id) return gui.pressed_button_mbtn;
    return 0;
}

uint32
gui_button(const char *title, int x, int y, int w, int h, uint32 mbtn_mask)
{
    gui_assert(title);
    uint32 id = fnv_hash32_from_str(title);
    return _gui_graphic_button(id, title, x, y, w, h, mbtn_mask);
}

uint32
gui_tex_button(const char *title, int x, int y, int w, int h, uint32 mbtn_mask)
{
    gui_assert(title);
    uint32 id = fnv_hash32_from_str(title);
    return _gui_graphic_tex_button(id, title, x, y, w, h, mbtn_mask);
}

uint32
gui_invisible_button(const char *title, int x, int y, int w, int h,
    uint32 mbtn_mask)
{
    gui_assert(title);
    uint32 id = fnv_hash32_from_str(title);
    return _gui_button_internal(id, x, y, w, h, mbtn_mask, 0, 0, 0);
}

int
gui_text(const char *text, int wrap, int x, int y)
    {return gui_text_s(text, wrap, x, y, 1.f);}

int
gui_textf(const char *fmt, int wrap, int x, int y, ...)
{
    if (!fmt || !gui.font || !sfont_is_loaded(gui.font)) return 1;

    int res;

    va_list args;
    va_start(args, y);
    res = vsnprintf(gui.fmt_buf, gui.fmt_buf_size, fmt, args);
    va_end(args);

    if (res >= gui.fmt_buf_size)
    {
        if (_gui_realloc_fmt_buf(res + 1) != 0)
            return 2;

        va_list args;
        va_start(args, y);
        stbsp_vsnprintf(gui.fmt_buf, gui.fmt_buf_size, fmt, args);
        va_end(args);
    }
    return gui_text(gui.fmt_buf, wrap, x, y) == 0 ? 0 : 3;
}

int
gui_text_s(const char *text, int wrap, int x, int y, float s)
{
    if (!text || !gui.font || !sfont_is_loaded(gui.font)) return 1;
    gui_win_t *win = GUI_WIN_STACK_TOP();
    int content_rect[4];
    GUI_COMPUTE_WIN_CONTENT_RECT(win, content_rect);
    int r = _gui_draw_con_draw_text(&win->draw_con, text, gui.font,
        (float)x, (float)y, content_rect, gui.origin, wrap, s, s,
        gui.text_color, gui.last_text_rect);
    if (!r) gui.last_text_origin = gui.origin;
    return r;
}

uint32
gui_get_active_text_input()
    {return gui.active_text_input;}

/* Returns true if this is the currently active text input field */
bool32
gui_text_input(int id, const char *text, int wrap, int x, int y, int w, int h )
{
    if (!text)
        return 0;

    int         tx, ty;
    gui_win_t   *p;
    uint32      ret = _gui_button_internal(id, x, y, w, h, 0, &tx, &ty, &p);
    gui_button_state_style_t *ss = &gui.button_style->normal;

    _gui_win_draw_quad(p, tx - 1, ty - 1, w + 1, h + 1, 0, 0, ss->br_col,
        GUI_FLIP_NONE, 0);
    _gui_win_draw_quad(p, tx + ss->bw_l, ty + ss->bw_t, w - ss->bw_l - ss-> bw_r,
        h - ss->bw_t - ss->bw_b, 0, 0, ss->bg_col, GUI_FLIP_NONE, 0);

    if (ret)
    {
        gui.active_text_input        = id;
        gui.input_indctr_timer       = 0;
        gui.input_indctr_active      = 1;
    }

    if (gui.input_indctr_active && id == gui.active_text_input)
        gui_textf(" %s|", wrap , x, y, text);
    else
        gui_textf(" %s", wrap , x, y, text);

    return id == gui.active_text_input;
}

int
gui_texture(tex_t *tex, float *clip, int x, int y)
{
    return gui_texture_scfr(tex, clip, x, y, 1.0f, 1.0f, gui_color_white,
           GUI_FLIP_NONE, 0.0f);
}

int
gui_texture_s(tex_t *tex, float *clip, int x, int y, float sx, float sy)
{
    return gui_texture_scfr(tex, clip, x, y, sx, sy, gui_color_white,
           GUI_FLIP_NONE, 0.0f);
}

int
gui_texture_c(tex_t *tex, float *clip, int x, int y, uint8 col[4])
{
    return gui_texture_scfr(tex, clip, x, y, 1.0f, 1.0f, col,
           GUI_FLIP_NONE, 0.0f);
}

int
gui_texture_f(tex_t *tex, float *clip, int x, int y, enum gui_flip_t flip)
{
    return gui_texture_scfr(tex, clip, x, y, 1.0f, 1.0f, gui_color_white,
           flip, 0.0f);
}

int
gui_texture_r(tex_t *tex, float *clip, int x, int y, float rot)
{
    return gui_texture_scfr(tex, clip, x, y, 1.0f, 1.0f, gui_color_white,
           GUI_FLIP_NONE, 0.0f);
}

int
gui_texture_sc(tex_t *tex, float *clip, int x, int y,
    float sx, float sy, uint8 col[4])
{
    return gui_texture_scfr(tex, clip, x, y, sx, sy, col,
           GUI_FLIP_NONE, 0.0f);
}

int
gui_texture_sf(tex_t *tex, float *clip, int x, int y, float sx, float sy,
    enum gui_flip_t flip)
{
    return gui_texture_scfr(tex, clip, x, y, sx, sy, gui_color_white,
           flip, 0.0f);
}

int
gui_texture_sr(tex_t *tex, float *clip, int x, int y, float sx, float sy,
    float rot)
{
    return gui_texture_scfr(tex, clip, x, y, sx, sy, gui_color_white,
        GUI_FLIP_NONE, rot);
}

int
gui_texture_cf(tex_t *tex, float *clip, int x, int y, uint8 col[4],
    enum gui_flip_t flip)
{
    return gui_texture_scfr(tex, clip, x, y, 1.0f, 1.0f, col,
           flip, 0.0f);
}

int
gui_texture_cr(tex_t *tex, float *clip, int x, int y, uint8 col[4],
    float rot)
{
    return gui_texture_scfr(tex, clip, x, y, 1.0f, 1.0f, col,
        GUI_FLIP_NONE, rot);
}

int
gui_texture_scf(tex_t *tex, float *clip, int x, int y,
    float sx, float sy, uint8 col[4], enum gui_flip_t flip)
{
    return gui_texture_scfr(tex, clip, x, y, sx, sy, col, flip, 0.0f);
}

int
gui_texture_scfr(tex_t *tex, float *clip, int x, int y,
    float sx, float sy, uint8 col[4], enum gui_flip_t flip, float rot)
{
    if (!tex || !tex_is_loaded(tex) || !col) return 0;

    float *uclip;
    float tmp_clip[4];

    if (clip)
        uclip = clip;
    else
    {
        tmp_clip[0] = 0.f;
        tmp_clip[1] = 0.f;
        tmp_clip[2] = (float)tex->w;
        tmp_clip[3] = (float)tex->h;
        uclip       = tmp_clip;
    }

    gui_win_t *p = GUI_WIN_STACK_TOP();

    int sw = (int)(sx * (float)(uclip[2] - uclip[0]));
    int sh = (int)(sy * (float)(uclip[3] - uclip[1]));
    int tx, ty;
    GUI_COMPUTE_QUAD_XY(&tx, &ty, gui.origin, x, y, sw, sh);

    int r = _gui_win_draw_quad(p, tx, ty, sw, sh, tex, uclip, col, flip, rot);

    if (!r)
    {
        gui.last_texture_origin     = gui.origin;
        gui.last_texture_rect[0]    = x;
        gui.last_texture_rect[1]    = y;
        gui.last_texture_rect[2]    = sw;
        gui.last_texture_rect[3]    = sh;
    }

    return r;
}

int
gui_quad(int x, int y, int w, int h, uint8 *col)
{
    gui_win_t *p = GUI_WIN_STACK_TOP();
    int crect[4];
    GUI_COMPUTE_WIN_CONTENT_RECT(p, crect);
    int tx, ty;
    _gui_compute_quad_parented_xy(&tx, &ty, gui.origin, crect, x,  y,  w, h);
    uint8 *tc = col ? col : gui_color_black;
    return _gui_win_draw_quad(p, tx, ty, w, h, 0, 0, tc, GUI_FLIP_NONE, 0);
}

void
gui_clicked_coord_inside_win(int *x, int *y)
    {*x = gui.coord_inside_win_x; *y = gui.coord_inside_win_y;}

void
gui_origin(enum gui_origin_t origin)
    {gui.origin = origin;}

void
gui_font(sfont_t *font)
    {gui.font = font;}

void
gui_text_color(uint r, uint g, uint b, uint a)
{
    gui.text_color[0] = r;
    gui.text_color[1] = g;
    gui.text_color[2] = b;
    gui.text_color[3] = a;
}

void
gui_win_style(gui_win_style_t *style)
    {gui.win_style = style ? style : &df_win_style;}

void
gui_button_style(gui_button_style_t *style)
    {gui.button_style = style ? style : &df_btn_style;}

void
gui_progressbar_style(gui_progressbar_style_t *style)
    {gui.progressbar_style = style ? style : &df_progressbar_style;}

void
gui_tex_button_style(gui_tex_button_style_t *style)
    {gui.tex_button_style = style ? style : &df_tex_btn_style;}

int gui_get_last_text_origin() {return gui.last_text_origin;}
void gui_get_last_text_rect(int rect[4])
    {if (rect) for (int i = 0; i < 4; ++i) rect[i]= gui.last_text_rect[i];}
int gui_get_last_text_x() {return gui.last_text_rect[0];}
int gui_get_last_text_y() {return gui.last_text_rect[1];}
int gui_get_last_text_w() {return gui.last_text_rect[2];}
int gui_get_last_text_h() {return gui.last_text_rect[3];}

int gui_get_last_button_origin()
    {return gui.last_button_origin;}
void gui_get_last_button_rect(int rect[4])
    {if (rect) for (int i = 0; i < 4; ++i) rect[i] = gui.last_button_rect[i];}
int gui_get_last_button_x() {return gui.last_button_rect[0];}
int gui_get_last_button_y() {return gui.last_button_rect[1];}
int gui_get_last_button_w() {return gui.last_button_rect[2];}
int gui_get_last_button_h() {return gui.last_button_rect[3];}

int gui_get_last_texture_origin()
    {return gui.last_texture_origin;}
void gui_get_last_texture_rect(int rect[4])
    {if (rect) for (int i = 0; i < 4; ++i) rect[i] = gui.last_texture_rect[i];}
int gui_get_last_texture_x() {return gui.last_texture_rect[0];}
int gui_get_last_texture_y() {return gui.last_texture_rect[1];}
int gui_get_last_texture_w() {return gui.last_texture_rect[2];}
int gui_get_last_texture_h() {return gui.last_texture_rect[3];}

int gui_get_num_open_wins()
    {return gui.wins_open_num ? gui.wins_open_num - 1 : 0;}

gui_win_style_t
gui_create_win_style()
{
    gui_win_style_t style;

    /* Window */
    GUI_SET_COLOR(style.inactive.bg_col, 13,  13,  13, 255);
    GUI_SET_COLOR(style.inactive.br_col, 38,  82, 179, 255);
    GUI_SET_COLOR(style.hovered.bg_col,  13,  53,  13, 255);
    GUI_SET_COLOR(style.hovered.br_col,  38, 122, 179, 255);
    GUI_SET_COLOR(style.active.bg_col,    5,   5,   5, 255);
    GUI_COPY_COLOR(style.active.br_col, gui_color_blue);

    style.inactive.bw_t             = 15;
    style.inactive.bw_b             = 1;
    style.inactive.bw_l             = 1;
    style.inactive.bw_r             = 1;
    style.inactive.title_origin     = GUI_TOP_LEFT;
    style.inactive.title_offset[0]  = 2;
    style.inactive.title_offset[1]  = 0;
    style.inactive.title_scale[0]   = 1.f;
    style.inactive.title_scale[1]   = 1.f;

    style.hovered.bw_t              = 15;
    style.hovered.bw_b              = 1;
    style.hovered.bw_l              = 1;
    style.hovered.bw_r              = 1;
    style.hovered.title_origin      = GUI_TOP_LEFT;
    style.hovered.title_offset[0]   = 2;
    style.hovered.title_offset[1]   = 0;
    style.hovered.title_scale[0]    = 1.f;
    style.hovered.title_scale[1]    = 1.f;

    style.active.bw_t               = 15;
    style.active.bw_b               = 1;
    style.active.bw_l               = 1;
    style.active.bw_r               = 1;
    style.active.title_origin       = GUI_TOP_LEFT;
    style.active.title_offset[0]    = 2;
    style.active.title_offset[1]    = 0;
    style.active.title_scale[0]     = 1.f;
    style.active.title_scale[1]     = 1.f;
    return style;
}

gui_button_style_t
gui_create_button_style()
{
    gui_button_style_t style;

    GUI_SET_COLOR(style.normal.bg_col,  13, 13, 13, 255);
    GUI_SET_COLOR(style.hovered.bg_col, 13, 43, 43, 255);
    GUI_SET_COLOR(style.pressed.bg_col, 13, 83, 13, 255);

    GUI_SET_COLOR(style.normal.br_col,  128, 13, 13, 255);
    GUI_SET_COLOR(style.hovered.br_col, 128, 13, 13, 255);
    GUI_SET_COLOR(style.pressed.br_col, 255, 13, 13, 255);

#   define SET_PROPS(state) \
        style.state.bw_t            = 1; \
        style.state.bw_b            = 1; \
        style.state.bw_l            = 1; \
        style.state.bw_r            = 1; \
        style.state.title_origin    = GUI_CENTER_CENTER; \
        style.state.title_offset[0] = 0; \
        style.state.title_offset[1] = -3; \
        style.state.title_scale[0]  = 1.f; \
        style.state.title_scale[1]  = 1.f; \
        style.state.font            = 0;

    SET_PROPS(normal);
    SET_PROPS(hovered);
    SET_PROPS(pressed);
#   undef SET_PROPS
    return style;
}

gui_tex_button_style_t
gui_create_tex_button_style()
{
    gui_tex_button_style_t style;

    int i;

#   define SET_PROPS(state) \
        style.state.tex                 = 0; \
        for (i = 0; i < 4; ++i) \
            style.state.clip[i]         = 0.f; \
        style.state.tex_offset[0]       = 0; \
        style.state.tex_offset[1]       = 0; \
        style.state.title_origin        = GUI_CENTER_CENTER; \
        style.state.title_offset[0]     = 0; \
        style.state.title_offset[1]     = 0; \
        style.state.title_scale[0]      = 1.f; \
        style.state.title_scale[1]      = 1.f; \
        style.state.font                = 0;

    SET_PROPS(normal);
    SET_PROPS(hovered);
    SET_PROPS(pressed);
#   undef SET_PROPS
    return style;
}

gui_progressbar_style_t
gui_create_progressbar_style()
{
    gui_progressbar_style_t bar;
    int i;

    GUI_SET_COLOR(bar.fill_col, 13, 83, 13, 255);
    GUI_SET_COLOR(bar.bg_col,  128, 13, 13, 255);

#   define SET_PROPS(bar) \
        bar.tex                         = 0; \
        for (i = 0; i < 4; ++i) \
            bar.clip[i]                 = 0.f; \
        bar.title_origin                = GUI_CENTER_CENTER; \
        bar.title_offset[0]             = 0; \
        bar.title_offset[1]             = 0; \
        bar.title_scale[0]              = 1.f; \
        bar.title_scale[1]              = 1.f; \
        bar.font                        = 0;

    SET_PROPS(bar);
#undef SET_PROPS
    return bar;
}


static uint32
_gui_graphic_button(uint32 id, const char *title, int x, int y, int w, int h, uint32 mbtn_mask)
{
    gui_win_t   *p;
    int         tx, ty;
    uint32 ret = _gui_button_internal(id, x, y, w, h, mbtn_mask, &tx, &ty, &p);

    /* Render */
    gui_button_state_style_t *ss;

    if (gui.pressed_button_id == id)
        ss = &gui.button_style->pressed;
    else if (gui.hovered_button == id)
        ss = &gui.button_style->hovered;
    else
        ss = &gui.button_style->normal;

    /* Border */
    _gui_win_draw_quad(p, tx, ty, w, h, 0, 0, ss->br_col, GUI_FLIP_NONE, 0);
    /* Background */
    _gui_win_draw_quad(p, tx + ss->bw_l, ty + ss->bw_t, w - ss->bw_l - ss->bw_r,
        h - ss->bw_t - ss->bw_b, 0, 0, ss->bg_col, GUI_FLIP_NONE, 0);

    /* Title */
    sfont_t *t_font = ss->font ? ss->font : gui.font;

    if (t_font && sfont_is_loaded(t_font))
    {
        int rect[] = {tx, ty, w, h};
        _gui_draw_con_draw_title_text(&p->draw_con, title, t_font,
            ss->title_offset[0], ss->title_offset[1], rect, ss->title_origin,
            ss->title_scale[0], ss->title_scale[1], gui_color_white);
    }
    return ret;
}

static uint32
_gui_graphic_tex_button(uint32 id, const char *title, int x, int y, int w, int h, uint32 mbtn_mask)
{
    gui_win_t   *p;
    int         tx, ty;
    uint32 ret = _gui_button_internal(id, x, y, w, h, mbtn_mask, &tx, &ty, &p);

    /* Render */
    gui_tex_button_state_style_t *ss;

    if (gui.pressed_button_id == id)
        ss = &gui.tex_button_style->pressed;
    else if (gui.hovered_button == id)
        ss = &gui.tex_button_style->hovered;
    else
        ss = &gui.tex_button_style->normal;

    if (!ss->tex)
        goto render_title;

    /* Background */
    int crect[4];
    GUI_COMPUTE_WIN_CONTENT_RECT(p, crect);

    float uclip[4];

    if (ss->clip[2] > 0.f && ss->clip[3] > 0.f)
    {
        for (int i = 0; i < 4; ++i)
            uclip[i] = ss->clip[i];
    } else
    {
        uclip[0] = 0.f;
        uclip[1] = 0.f;
        uclip[2] = ss->tex->w;
        uclip[3] = ss->tex->h;
    }

    int sw = (int)(uclip[2] - uclip[0]);
    int sh = (int)(uclip[3] - uclip[1]);

    int tex_x, tex_y;
    _gui_compute_quad_parented_xy(&tex_x, &tex_y, gui.origin,
        crect, x, y, sw, sh);

    _gui_win_draw_quad(p, tex_x, tex_y, sw, sh, ss->tex, uclip,
        gui_color_white, GUI_FLIP_NONE, 0);

    /* Title */
    render_title:
    {
        sfont_t *t_font = ss->font ? ss->font : gui.font;
        if (t_font && sfont_is_loaded(t_font))
        {
            int rect[] = {tx, ty, w, h};
            _gui_draw_con_draw_title_text(&p->draw_con, title, t_font,
                ss->title_offset[0], ss->title_offset[1], rect,
                ss->title_origin, ss->title_scale[0], ss->title_scale[1],
                gui_color_white);
        }
        return ret;
    }
}


static gui_win_t *
_gui_get_win_by_id(uint32 id)
{
    gui_win_t *wins = gui.wins;
    for (int i = 0; i < gui.win_num; ++i)
        if (wins[i].id == id)
            return &wins[i];
    return 0;
}

static gui_win_t *
_gui_create_win(uint32 id)
{
    if (gui.win_num == GUI_MAX_WINS)
        return 0;
    gui_win_t *win = &gui.wins[gui.win_num];
    if (_gui_draw_con_init(&win->draw_con, 16))
        return 0;
    win->last_hovered   = 0;
    win->last_active    = 0;
    win->id             = id;
    gui.win_num++;
    return win;
}

static void
_gui_compute_quad_parented_xy(int *ret_x, int *ret_y, enum gui_origin_t o,
    int *p, int x,  int y,  int w,  int  h)
{
    switch (o)
    {
        case GUI_TOP_LEFT:
            *ret_x = p[0] + x;
            *ret_y = p[1] + y;
            break;
        case GUI_TOP_RIGHT:
            *ret_x = p[0] + p[2] - w - x;
            *ret_y = p[1] + y;
            break;
        case GUI_TOP_CENTER:
            *ret_x = p[0] + p[2] / 2 - w / 2 + x;
            *ret_y = p[1] + y;
            break;
        case GUI_BOTTOM_LEFT:
            *ret_x = p[0] + x;
            *ret_y = p[1] + p[3] - y - h;
            break;
        case GUI_BOTTOM_RIGHT:
            *ret_x = p[0] + p[2] - w - x;
            *ret_y = p[1] + p[3] - y - h;
            break;
        case GUI_BOTTOM_CENTER:
            *ret_x = p[0] + p[2] / 2 - w / 2 + x;
            *ret_y = p[1] + p[3] - y - h;
            break;
        case GUI_CENTER_LEFT:
            *ret_x = p[0] + x;
            *ret_y = p[1] + p[3] / 2 - h / 2 + y;
            break;
        case GUI_CENTER_RIGHT:
            *ret_x = p[0] + p[2] - w - x;
            *ret_y = p[1] + p[3] / 2 - h / 2 + y;
            break;
        case GUI_CENTER_CENTER:
            *ret_x = p[0] + p[2] / 2 - w / 2 + x;
            *ret_y = p[1] + p[3] / 2 - h / 2 + y;
            break;
        default:
            gui_assert(0);
    }
}

static int
_gui_win_draw_quad(gui_win_t *win, int x, int y, int w, int h, tex_t *tex,
    float *clip, uint8 *color, enum gui_flip_t flip, float rot)
{
    return _gui_win_draw_quad_f(win, (float)x, (float)y, (float)w, (float)h,
            tex, clip, color, flip, rot);
}

static int
_gui_win_draw_quad_f(gui_win_t *win, float x, float y, float w, float h, tex_t *tex,
    float *clip, uint8 *color, enum gui_flip_t flip, float rot)
{
    /* We have to fetch the index because the memory address could change
     * as we allocate vertices */
    gui_win_t *cwin = win;
    float *verts = _gui_draw_con_reserve_verts(&win->draw_con, 4);
    if (!verts)
        return 1;
    _gui_write_quad_verts(verts, tex, clip, x, y, w, h, color, flip);
    GUI_ROTATE_QUAD(verts, x, y, w, h, rot);
    if (_gui_draw_con_push_draw_cmd(&cwin->draw_con, 4, tex) == 0)
        return 0;
    return 2;
}

static int
_gui_draw_con_draw_text(gui_draw_con_t *con, const char *txt, sfont_t *sf,
    float x, float y, int *parent_dim, int origin, int wrap,
    float sx, float sy, uint8 *col, int *ret_rect)
{
    int len = _gui_strlen(txt);
    if (_gui_draw_con_push_draw_cmd(con, len * 4, &sf->tex) != 0)
        return 2;

    float *verts = _gui_draw_con_reserve_verts(con, len * 4);
    if (!verts)
        return 1;

    float bx, by;

    if (origin == GUI_TOP_LEFT)
    {
        bx = (float)parent_dim[0] + x;
        by = (float)parent_dim[1] + y;
    } else
    {
        /* If there's no wrapping, wrap to parent dimensions */
        float quad_wrap = (float)(wrap && wrap < parent_dim[2] \
            ? wrap : parent_dim[2]);

        int w, h, tmp_x, tmp_y;
        _gui_compute_text_block_wh(txt, sf, quad_wrap, sx, sy, &w, &h);
        _gui_compute_quad_parented_xy(&tmp_x, &tmp_y, origin,
            parent_dim, (int)x,  (int)y,  w, h);
        bx = (float)tmp_x;
        by = (float)tmp_y;
    }

    by += sf->v_advance * sy;

    sfont_glyph_t   *g;
    float           rx, ry, w, h;

    tex_t *tex          = &sf->tex;
    float max_x         = wrap && wrap < parent_dim[2] ? bx + (float)wrap \
        : bx + (float)parent_dim[2];
    float *write_verts  = verts;

    int             num_drawn   = 0;
    float           px          = bx;
    float           py          = by;
    float           v_adv       = sf->v_advance * sy;
    float           rect_w      = 0.f;

    for (const char *c = txt; *c; ++c)
    {
        if (*c == '\n')
        {
            if (px > rect_w) rect_w = px;
            px = bx;
            py += v_adv;
            /* Break out if max vertical height is reached */
            /* Note: can't do this before we can resize vert containers */
            /*if (py + v_adv > max_y)*/
                /*break;*/
            continue;
        }

        g   = SFONT_GLYPH(sf, (int)*c);
        w   = (g->clip[2] - g->clip[0]) * sx;
        h   = (g->clip[3] - g->clip[1]) * sy;

        if (c != txt && px + (sx * g->advance) > max_x)
        {
            if (px > rect_w) rect_w = px;
            px = bx;
            py += v_adv;
        }

        rx  = px;
        ry  = (py - h + g->y_off * sy);

        GUI_WRITE_TEX_QUAD_VERTS(write_verts, tex, g->clip, rx, ry, w, h, col);

        px += (g->advance * sx);
        num_drawn++;
        write_verts = verts + (num_drawn * GUI_NUM_FLOATS_PER_QUAD);
    }

    gui_assert(num_drawn == len);

    if (ret_rect)
    {
        ret_rect[0] = (int)x;
        ret_rect[1] = (int)y;
        ret_rect[2] = wrap ? wrap : (int)rect_w;
        ret_rect[3] = (int)(py + v_adv - by);
    }
    return 0;
}

static int
_gui_draw_con_draw_title_text(gui_draw_con_t *con, const char *txt,
    sfont_t *sf, int x, int y, int *parent_dim, int origin, float sx, float sy,
    uint8 *col)
{
    int len = _gui_title_strlen(txt);
    if (_gui_draw_con_push_draw_cmd(con, len * 4, &sf->tex) != 0)
        return 1;

    float *verts = _gui_draw_con_reserve_verts(con, len * 4);
    if (!verts)
        return 1;

    float bx, by;

    switch (origin)
    {
        case GUI_TOP_LEFT:
        {
            bx = (float)parent_dim[0] + (float)x;
            by = (float)parent_dim[1] + (float)y;
        }
            break;
        default:
        {
            gui_assert(origin < NUM_GUI_ORIGINS);
            int w, h, tmp_x, tmp_y;
            w = _gui_compute_title_text_line_w(txt, sf, sx);
            h = (int)(sy * sf->v_advance);
            _gui_compute_quad_parented_xy(&tmp_x, &tmp_y, origin, parent_dim,
                x,  y,  w, h);
            bx = (float)tmp_x;
            by = (float)tmp_y;
        }
    }

    by += sf->v_advance * sy;

    sfont_glyph_t   *g;
    float           rx, ry, w, h;

    tex_t           *tex            = &sf->tex;
    float           *write_verts    = verts;
    float           px              = bx;
    float           py              = by;
    int             num_drawn       = 0;

    for (const char *c = txt;
         *c && *c != '\n' && !(*c == '#' && *(c + 1) == '#');
         ++c)
    {
        g   = SFONT_GLYPH(sf, (int)*c);
        w   = (g->clip[2] - g->clip[0]) * sx;
        h   = (g->clip[3] - g->clip[1]) * sy;
        rx  = px;
        ry  = (py - h + g->y_off * sy);
        GUI_WRITE_TEX_QUAD_VERTS(write_verts, tex, g->clip, rx, ry, w, h, col);
        px += (g->advance * sx);
        num_drawn++;
        write_verts = verts + (num_drawn * GUI_NUM_FLOATS_PER_QUAD);
    }
    gui_assert(num_drawn == len);
    return 0;
}

static void
_gui_write_quad_verts(float *verts, tex_t *tex, float *tex_clip,
    float x, float y, float w, float h, uint8 *color, enum gui_flip_t flip)
{
    uint8 *col = color ? color : gui_color_white;

    if (tex)
    {
        float full_clip[4];
        float *clip;

        if (tex_clip)
            clip = tex_clip;
        else
        {
            full_clip[0] = x;
            full_clip[1] = y;
            full_clip[2] = tex->w;
            full_clip[3] = tex->h;
            clip = full_clip;
        }

        GUI_WRITE_FLIPPED_TEX_QUAD_VERTS(verts, tex, clip, x, y, w, h, col, flip);
    } else
    {
        verts[ 0]   = x;
        verts[ 1]   = y;
        verts[ 5]   = x + w;
        verts[ 6]   = y;
        verts[10]   = x;
        verts[11]   = y + h;
        verts[15]   = x + w;
        verts[16]   = y + h;
        uint8 *bverts = (uint8*)verts;
        bverts[16]  = col[0];
        bverts[17]  = col[1];
        bverts[18]  = col[2];
        bverts[19]  = col[3];
        bverts[36]  = col[0];
        bverts[37]  = col[1];
        bverts[38]  = col[2];
        bverts[39]  = col[3];
        bverts[56]  = col[0];
        bverts[57]  = col[1];
        bverts[58]  = col[2];
        bverts[59]  = col[3];
        bverts[76]  = col[0];
        bverts[77]  = col[1];
        bverts[78]  = col[2];
        bverts[79]  = col[3];
    }
}

static void
_gui_sort_win_children(gui_win_t *win)
{
    gui_assert(win->child_list_index >= 0);
    int_darr_t **child_list     = &gui.win_child_lists[win->child_list_index];
    int        **child_indices  = child_list;

    int         num_children    = (int)darr_num(*child_list);
    int         end             = num_children - 1;
    bool32      swapped         = 1;
    int         i, tmp;
    gui_win_t   *c, *oc;

    while (swapped)
    {
        swapped = 0;
        for (i = 0; i < end; ++i)
        {
            c   = &gui.wins[(*child_list)[i]];
            oc  = &gui.wins[(*child_list)[i + 1]];
            gui_assert(c != oc);
            if (oc->last_active >= c->last_active)
                continue;
            tmp                     = (*child_list)[i];
            (*child_list)[i]        = (*child_list)[i + 1];
            (*child_list)[i + 1]    = tmp;
            swapped                 = 1;
        }
    }

    for (i = 0; i < num_children; ++i)
    {
        c = &gui.wins[(*child_indices)[i]];
        gui_assert(c != win);
        child_list = GUI_WIN_CHILD_LIST(c);
        if (child_list && darr_num(*child_list) > 1)
            _gui_sort_win_children(c);
    }
}

static void
_gui_add_win_to_draw_list(gui_win_t *win, gui_win_t *all_wins,
    gui_draw_list_t *lists, int *num_lists)
{
    int index = *num_lists;

    if (darr_num(win->draw_con.vert_floats) > 0)
    {
        gui_win_state_style_t *s = win->style;
        int border[4];
        if (s)
        {
            border[0] = s->bw_l;
            border[1] = s->bw_t;
            border[2] = s->bw_r;
            border[3] = s->bw_b;
        } else
            memset(border, 0, sizeof(border));
        for (int j = 0; j < 4; ++j)
            lists[index].clip[j] = win->dim[j];
        lists[index].cmds           = win->draw_con.draw_cmds;
        lists[index].num_cmds       = darr_num(win->draw_con.draw_cmds);
        lists[index].verts          = win->draw_con.vert_floats;
        lists[index].num_verts      =
            darr_num(win->draw_con.vert_floats) / GUI_NUM_FLOATS_PER_QUAD;
        *num_lists = index + 1;
    }

    int_darr_t **child_list = GUI_WIN_CHILD_LIST(win);
    if (!child_list)
        return;
    uint32 num = darr_num(*child_list);
    for (uint32 i = 0; i < num; ++i)
        _gui_add_win_to_draw_list(&all_wins[(*child_list)[i]], all_wins, lists,
            num_lists);
}

static inline bool32
_gui_test_next_active_win_parenthood()
{
    for (int i = 0; i < gui.win_stack_num; ++i)
        if (gui.win_stack[i] == gui.next_act_win.index)
            return 1;
    return 0;
}

static inline bool32
_gui_test_next_hovered_win_parenthood()
{
    for (int i = 0; i < gui.win_stack_num; ++i)
        if (gui.win_stack[i] == gui.next_hov_win.index)
            return 1;
    return 0;
}

int
_gui_draw_con_init(gui_draw_con_t *con, int num_cmds)
{
    darr_reserve(con->draw_cmds, num_cmds);
    darr_reserve(con->vert_floats, num_cmds * 16 * GUI_NUM_FLOATS_PER_QUAD);
    return 0;
}

static inline void
_gui_draw_con_clear(gui_draw_con_t *con)
{
    darr_clear(con->draw_cmds);
    darr_clear(con->vert_floats);
}

static float *
_gui_draw_con_reserve_verts(gui_draw_con_t *con, int num_verts)
{
    int index = darr_num(con->vert_floats);
    int num_new = num_verts * GUI_NUM_FLOATS_PER_VERT;
    darr_reserve(con->vert_floats, index + num_new);
    darr_head(con->vert_floats)->num += num_new;
    return con->vert_floats + index;
}

static int
_gui_draw_con_push_draw_cmd(gui_draw_con_t *con, int num_verts, tex_t *tex)
{
    uint32 tex_id = tex ? tex->id : 0;

    /* Can always count on there being room for at least one */
    if (!darr_num(con->draw_cmds))
    {
        gui_draw_cmd_t cmd;
        cmd.tex         = tex_id;
        cmd.num_verts   = num_verts;
        darr_push(con->draw_cmds, cmd);
        return 0;
    }

    int index = darr_num(con->draw_cmds) - 1;

    if (con->draw_cmds[index].tex == tex_id)
    {
        con->draw_cmds[index].num_verts += num_verts;
        return 0;
    }

    gui_draw_cmd_t cmd;
    cmd.tex         = tex_id;
    cmd.num_verts   = num_verts;
    darr_push(con->draw_cmds, cmd);
    return 0;
}

static int
_gui_compute_title_text_line_w(const char *txt, sfont_t *f, float x_scale)
{
    float w = 0;

    for (const char *c = txt;
         *c && *c != '\n' && !(*c == '#' && c > txt && *(c - 1) == '#');
         ++c)
    {
        w += SFONT_ADV(f, (int)*c) * x_scale;
    }

    return (int)w;
}

static void
_gui_compute_text_block_wh(const char *txt, sfont_t *sf, float wrap, float sx,
    float sy, int *ret_w, int *ret_h)
{
    float v_adv         = sy * sf->v_advance;
    float longest_line  = 0;
    float w             = 0;
    float h             = *txt ? v_adv : 0;
    sfont_glyph_t *g;
    float h_adv;

    for (const char *c = txt; *c; ++c)
    {
        if (*c == '\n')
        {
            if (w > longest_line) longest_line = w;
            w = 0;
            h += v_adv;
            continue;
        }

        g       = SFONT_GLYPH(sf, (int)*c);
        h_adv   = g->advance * sx;

        if (w + h_adv > wrap && c != txt)
        {
            if (w > longest_line) longest_line = w;
            w = 0;
            h += v_adv;
        }

        w += h_adv;
    }

    *ret_w = (int)(longest_line > w ? longest_line : w);
    *ret_h = (int)h;
}

static inline int
_gui_strlen(const char *txt)
{
    int len = 0;
    if (txt) {for (const char *c = txt; *c; ++c) {if (*c != '\n') ++len;}}
    return len;
}

static inline int
_gui_title_strlen(const char *txt)
{
    int len = 0;
    if (!txt)
        return len;
    for (const char *c = txt;
         *c && *c != '\n' && !(*c == '#' && *(c + 1) == '#');
         ++c)
        ++len;
    return len;
}

static int
_gui_realloc_fmt_buf(int new_size)
{
    int suggested_sz = gui.fmt_buf_size * 150 / 100;
    if (suggested_sz > new_size)
        new_size = suggested_sz;
    gui_assert(new_size > gui.fmt_buf_size);
    char *m = gui_realloc(gui.fmt_buf, new_size);
    if (!m)
        return 1;
    gui.fmt_buf = m;
    gui.fmt_buf_size = new_size;
    gui_debug_printf("%s called.\n", __func__);
    return 0;
}

static uint32
_gui_button_internal(uint32 id, int x, int y, int w, int h,
    uint32 mbtn_mask, int *ret_tx, int *ret_ty, gui_win_t **ret_parent)
{
    int tx, ty;
    GUI_COMPUTE_QUAD_XY(&tx, &ty, gui.origin, x, y, w, h);

    gui_win_t *p = GUI_WIN_STACK_TOP();

    uint32 ret = 0;

    if (GUI_TEST_MOUSE_RECT(tx, ty, w, h))
    {
        uint32 t_mbtn_mask = mbtn_mask == 0 ? GUI_MB_LEFT : mbtn_mask;

        /* Mouse was released on top of this button */
        if (gui.pressed_button_id == id
        && gui.mbtns_up_now & gui.pressed_button_mbtn)
        {
            ret = gui.mbtns_up_now & gui.pressed_button_mbtn;
            gui.released_button_id = id;
        } else /* Mouse went down on top of this button */
        if ((t_mbtn_mask & gui.mbtns_down_now)
        &&  (gui.next_prs_btn.id == 0
             || gui.next_prs_btn.last_active <= p->last_active))
        {
            gui.next_prs_btn.id             = id;
            gui.next_prs_btn.parent_id      = p->id;
            gui.next_prs_btn.last_active    = p->last_active;
            gui.next_prs_btn.mouse_btn      = t_mbtn_mask;
            gui.clicked_this_frame          = 1;
        } else
        if (gui.next_hov_btn.id == 0
        || (gui.next_hov_btn.last_active <= p->last_active))
        {
            gui.next_hov_btn.id             = id;
            gui.next_hov_btn.parent_id      = p->id;
            gui.next_hov_btn.last_active    = p->last_active;
        }
    }

    if (ret_tx)     *ret_tx     = tx;
    if (ret_ty)     *ret_ty     = ty;
    if (ret_parent) *ret_parent = p;

    gui.last_button_origin  = gui.origin;
    gui.last_button_rect[0] = x;
    gui.last_button_rect[1] = y;
    gui.last_button_rect[2] = w;
    gui.last_button_rect[3] = h;
    return ret;
}

/* Returns < 0 on failure */
static int
_gui_claim_win_child_list()
{
    int num = darr_num(gui.win_child_lists);
    if (num == GUI_MAX_WINS)
        return -1;
    darr_clear(gui.win_child_lists[num]);
    _darr_head(gui.win_child_lists)->num = num + 1;
    return num;
}
